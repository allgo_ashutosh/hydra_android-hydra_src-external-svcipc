#ifndef _SLOG_H_
#define _SLOG_H_

#define _SLOG_SHUTDOWN	0	//Shut down the system NOW (e.g. for OEM use)
#define _SLOG_CRITICAL	1	//Unexpected unrecoverable error (e.g. hard disk error)
#define _SLOG_ERROR	    2	//Unexpected recoverable error (e.g. needed to reset a hardware controller)
#define _SLOG_WARNING	3	//Expected error (e.g. parity error on a serial port)
#define _SLOG_NOTICE	4	//Warnings (e.g. out of paper)
#define _SLOG_INFO	    5	//Information (e.g. printing page 3)
#define _SLOG_DEBUG1	6	//Debug messages (normal detail)
#define _SLOG_DEBUG2	7	//Debug messages (fine detail)


#define _SLOG_SETCODE(major, minor)     (major + minor)

int slogf( int opcode,
           int severity, 
           const char * fmt,
           ... );

#endif
