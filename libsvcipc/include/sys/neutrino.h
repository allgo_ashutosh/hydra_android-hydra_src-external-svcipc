#ifndef NEUTRINO_H
#define NEUTRINO_H

#include "svcipc/svcipc_types.h"

union sigval { 
    int sival_int; 
    void * sival_ptr; 
}; 

struct _pulse {
    uint16_t                    type;
    uint16_t                    subtype;
    int8_t                      code;
    uint8_t                     zero[3];
    union sigval                value;
    int32_t                     scoid;
};

#define _PULSE_CODE_MINAVAIL   100
#define _NTO_SIDE_CHANNEL      102

int MsgSendPulse ( int coid,
                   int priority,
                   int code,
                   int value );

int MsgReceivePulse( int chid,
                     void * pulse,
                     int bytes,
                     void* info );
#endif
