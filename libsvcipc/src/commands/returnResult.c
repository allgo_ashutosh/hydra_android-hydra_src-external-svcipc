/*
 * returnResult.c
 *
 *  Created on: Apr 8, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "context.h"
#include "dispatcher.h"

// Handles both returnResult and returnError commands.

typedef struct
{
   Command header;
   Context *context;
   char *result;
   char *errName;
   char *errMsg;
   SVCIPC_tStatusCallback onStatus;
   SVCIPC_tUserToken token;
} ReturnCommand;

static void freeCommand(ReturnCommand *cmd)
{
   if (cmd)
   {
      free(cmd->result);
      free(cmd->errName);
      free(cmd->errMsg);
      free(cmd);
   }
}

//////////////////////////////////////////////////////////////////////
// Execute command

static void execute(ReturnCommand *cmd, Connection *conn)
{
   TRACE_INFO("execute returnResult conn=%p", conn);

   SVCIPC_tError errCode;
   if (cmd->result)
      errCode = svcipcSendReply(cmd->context, cmd->result);
   else
      errCode = svcipcSendError(cmd->context, cmd->errName, cmd->errMsg);

   if (cmd->header.sem)
   {
      svcipcPostResult(&cmd->header, errCode);
   }
   else if (cmd->onStatus)
   {
      SVCIPC_tCallbackStatus status =
      { errCode, 0, 0 };
      cmd->onStatus(&status, cmd->token);
   }

   freeCommand(cmd);
}

//////////////////////////////////////////////////////////////////////
// returnResult

static ReturnCommand *newResultCommand(SVCIPC_tReqContext context, SVCIPC_tConstStr result, SVCIPC_tError *errCode)
{
   if (context)
   {
      ReturnCommand *cmd = svcipcCommand(sizeof(ReturnCommand), execute, 0);
      if (cmd)
      {
         cmd->context = context;
         cmd->result = strdup(result ? result : "{}");
         if (cmd->result)
            return cmd;
         freeCommand(cmd);
      }
      TRACE_WARN("SVCIPC_returnResult out of memory");
      *errCode = SVCIPC_ERROR_NO_MEMORY;
   }
   else
   {
      TRACE_WARN("SVCIPC_returnResult context=NULL");
      *errCode = SVCIPC_ERROR_BAD_ARGS;
   }
   return 0;
}

SVCIPC_tError SVCIPC_returnResult(SVCIPC_tReqContext context, SVCIPC_tConstStr result)
{
   SVCIPC_tError errCode;
   ReturnCommand *cmd = newResultCommand(context, result, &errCode);
   if (cmd)
   {
      return svcipcSubmitCommandAndWait(&cmd->header);
   }
   else
   {
      return errCode;
   }
}

SVCIPC_API SVCIPC_tError SVCIPC_asyncReturnResult(SVCIPC_tReqContext context, SVCIPC_tConstStr result,
      SVCIPC_tStatusCallback onStatus, SVCIPC_tUserToken token)
{
   SVCIPC_tError errCode;
   ReturnCommand *cmd = newResultCommand(context, result, &errCode);
   if (cmd)
   {
      cmd->onStatus = onStatus;
      cmd->token = token;
      return svcipcSubmitCommand(&cmd->header);
   }
   else
   {
      return errCode;
   }
}

//////////////////////////////////////////////////////////////////////
// returnError

static ReturnCommand *newErrorCommand(SVCIPC_tReqContext context, SVCIPC_tConstStr errName, SVCIPC_tConstStr errMsg,
      SVCIPC_tError *errCode)
{
   if (context)
   {
      ReturnCommand *cmd = svcipcCommand(sizeof(ReturnCommand), execute, 0);
      if (cmd)
      {
         cmd->context = context;
         cmd->errName = strdup(errName ? errName : SVCIPC_INTERFACE_ERROR_NAME);
         cmd->errMsg = strdup(errMsg ? errMsg : "");
         if (cmd->errName && cmd->errMsg)
            return cmd;
         freeCommand(cmd);
      }
      TRACE_WARN("SVCIPC_returnError out of memory");
      *errCode = SVCIPC_ERROR_NO_MEMORY;
   }
   else
   {
      TRACE_WARN("SVCIPC_returnError context=NULL");
      *errCode = SVCIPC_ERROR_BAD_ARGS;
   }
   return 0;
}

SVCIPC_tError SVCIPC_returnError(SVCIPC_tReqContext context, SVCIPC_tConstStr errName, SVCIPC_tConstStr errMsg)
{
   SVCIPC_tError errCode;
   ReturnCommand *cmd = newErrorCommand(context, errName, errMsg, &errCode);
   if (cmd)
   {
      return svcipcSubmitCommandAndWait(&cmd->header);
   }
   else
   {
      return errCode;
   }
}

SVCIPC_API SVCIPC_tError SVCIPC_asyncReturnError(SVCIPC_tReqContext context, SVCIPC_tConstStr errName,
      SVCIPC_tConstStr errMsg, SVCIPC_tStatusCallback onStatus, SVCIPC_tUserToken token)
{
   SVCIPC_tError errCode;
   ReturnCommand *cmd = newErrorCommand(context, errName, errMsg, &errCode);
   if (cmd)
   {
      cmd->onStatus = onStatus;
      cmd->token = token;
      return svcipcSubmitCommand(&cmd->header);
   }
   else
   {
      return errCode;
   }
}
