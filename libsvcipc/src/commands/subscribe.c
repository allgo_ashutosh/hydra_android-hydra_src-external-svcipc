/*
 * subscribe.c
 *
 *  Created on: Apr 7, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "dispatcher.h"
#include "subscription.h"

typedef struct
{
   Command header;
   char *busName;
   char *objPath;
   char *sigName;
   SVCIPC_tSubscriptionCallback onSubscription;
   SVCIPC_tUserToken token;
   SVCIPC_tSigSubHnd *subHnd;
   Subscription *subscription;

} SubscribeCommand;

static void freeCommand(SubscribeCommand *cmd)
{
   if (cmd)
   {
      svcipcFreeSubscription(cmd->subscription);
      free(cmd->busName);
      free(cmd->objPath);
      free(cmd->sigName);
      free(cmd);
   }
}

////////////////////////////////////////////////////////////
// Dispatch command results

static void dispatchResult(SubscribeCommand *cmd, int errCode, const char *errName, const char *errMsg,
      Subscription *subscription)
{
   if (cmd->header.sem)
   {
      *cmd->subHnd = subscription;
      svcipcPostResult(&cmd->header, errCode);
   }
   else if (cmd->onSubscription)
   {
      SVCIPC_tCallbackStatus status =
      { errCode, errName, errMsg };
      cmd->onSubscription(&status, subscription, cmd->token);
   }

   freeCommand(cmd);
}

////////////////////////////////////////////////////////////
// Process pending command

static void onPendingCallNotify(DBusPendingCall *call, void *data)
{
   SubscribeCommand *cmd = data;

   DBusMessage *reply = dbus_pending_call_steal_reply(call);
   if (reply)
   {
      int replyType = dbus_message_get_type(reply);

      if (replyType == DBUS_MESSAGE_TYPE_METHOD_RETURN)
      {
         // give the subscription to the connection
         Subscription *sub = cmd->subscription;
         svcipcAddSubscription(cmd->subscription);
         cmd->subscription = 0;

         dispatchResult(cmd, SVCIPC_ERROR_NONE, 0, 0, sub);
      }
      else if (replyType == DBUS_MESSAGE_TYPE_ERROR)
      {
         const char *errName = dbus_message_get_error_name(reply);
         const char *errMsg = 0;
         if (!dbus_message_get_args(reply, 0, DBUS_TYPE_STRING, &errMsg, DBUS_TYPE_INVALID))
         {
            TRACE_WARN("onPendingCallNotify: Failed to extract error message");
         }
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, errName, errMsg, 0);
      }
      else
      {
         TRACE_WARN("onPendingCallNotify: Received unexpected D-Bus message type (%d)", replyType);
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, SVCIPC_ERR_NAME_DBUS, "Unknown message reply type", 0);
      }

      // Free the reply message
      dbus_message_unref(reply);
   }
   else
   {
      dispatchResult(cmd, SVCIPC_ERROR_INTERNAL, SVCIPC_ERR_NAME_INTERNAL, "Failed to retrieve reply message", 0);
   }
}

////////////////////////////////////////////////////////////

static DBusMessage *buildMessage(SubscribeCommand *cmd)
{
   DBusMessage* msg = dbus_message_new_method_call(DBUS_SERVICE_DBUS, DBUS_PATH_DBUS, DBUS_INTERFACE_DBUS, "AddMatch");
   if (msg)
   {
      const char *rule = cmd->subscription->rule;
      dbus_message_set_no_reply(msg, FALSE);
      if (!dbus_message_append_args(msg, DBUS_TYPE_STRING, &rule, DBUS_TYPE_INVALID))
      {
         dbus_message_unref(msg);
         return 0;
      }
   }
   return msg;
}

static void execute(SubscribeCommand *cmd, Connection *conn)
{
   TRACE_INFO("execute subscribeOwnerChanged conn=%p", conn);

   if (!svcipcIsConnected(conn))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NOT_CONNECTED, SVCIPC_ERR_NAME_NOT_CONNECTED, "Not connected", 0);
      return;
   }

   DBusMessage* msg = buildMessage(cmd);
   if (!msg)
   {
      dispatchResult(cmd, SVCIPC_ERROR_NO_MEMORY, SVCIPC_ERR_NAME_NO_MEMORY, "Out of memory", 0);
      return;
   }

   DBusConnection *dbusConn = svipcGetDBusConnection(conn);
   DBusPendingCall *call;
   if (!(dbus_connection_send_with_reply(dbusConn, msg, &call, -1) && dbus_pending_call_set_notify(call,
         onPendingCallNotify, cmd, 0)))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NO_MEMORY, SVCIPC_ERR_NAME_NO_MEMORY, "Out of memory", 0);
   }

   dbus_message_unref(msg);
}

////////////////////////////////////////////////////////////
// Subscribe to a service signal

static SubscribeCommand *newSignalCommand(SVCIPC_tConnection conn, SVCIPC_tConstStr objPath, SVCIPC_tConstStr sigName,
      SVCIPC_tSignalCallback onSignal, SVCIPC_tUserToken token)
{
   SubscribeCommand *cmd = svcipcCommand(sizeof(SubscribeCommand), execute, conn);
   if (cmd)
   {
      cmd->objPath = strdup(objPath);
      cmd->sigName = strdup(sigName);
      if (cmd->objPath && cmd->sigName)
      {
         cmd->subscription = svcipcNewSignalSubscription(conn, objPath, sigName, onSignal, token);
         if (cmd->subscription)
            return cmd;
      }
      freeCommand(cmd);
   }
   return 0;
}

SVCIPC_tError SVCIPC_subscribe(SVCIPC_tConnection conn, SVCIPC_tConstStr objPath, SVCIPC_tConstStr sigName,
      SVCIPC_tSignalCallback onSignal, SVCIPC_tUserToken token, SVCIPC_tSigSubHnd* subHnd)
{
   if (!(objPath && sigName && onSignal && subHnd))
      return SVCIPC_ERROR_BAD_ARGS;

   SubscribeCommand *cmd = newSignalCommand(conn, objPath, sigName, onSignal, token);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->subHnd = subHnd;
   return svcipcSubmitCommandAndWait(&cmd->header);
}

SVCIPC_tError SVCIPC_asyncSubscribe(SVCIPC_tConnection conn, SVCIPC_tConstStr objPath, SVCIPC_tConstStr sigName,
      SVCIPC_tSignalCallback onSignal, SVCIPC_tSubscriptionCallback onSubscription, SVCIPC_tUserToken token)
{
   if (!(objPath && sigName && onSignal))
      return SVCIPC_ERROR_BAD_ARGS;

   SubscribeCommand *cmd = newSignalCommand(conn, objPath, sigName, onSignal, token);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->onSubscription = onSubscription;
   cmd->token = token;
   return svcipcSubmitCommand(&cmd->header);
}

////////////////////////////////////////////////////////////
// Subscribe to owner changed notification

static SubscribeCommand *newOwnerCommand(SVCIPC_tConnection conn, SVCIPC_tConstStr busName,
      SVCIPC_tNameOwnerChangedCallback onOwnerChanged, SVCIPC_tUserToken token)
{
   SubscribeCommand *cmd = svcipcCommand(sizeof(SubscribeCommand), execute, conn);
   if (cmd)
   {
      cmd->busName = strdup(busName ? busName : "");
      if (cmd->busName)
      {
         cmd->subscription = svcipcNewOwnerChangedSubscription(conn, busName, onOwnerChanged, token);
         if (cmd->subscription)
            return cmd;
      }
      freeCommand(cmd);
   }
   return 0;
}

SVCIPC_tError SVCIPC_subscribeOwnerChanged(SVCIPC_tConnection conn, SVCIPC_tConstStr busName,
      SVCIPC_tNameOwnerChangedCallback onOwnerChanged, SVCIPC_tUserToken token, SVCIPC_tSigSubHnd* subHnd)
{
   if (!(onOwnerChanged && subHnd))
      return SVCIPC_ERROR_BAD_ARGS;

   SubscribeCommand *cmd = newOwnerCommand(conn, busName, onOwnerChanged, token);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->subHnd = subHnd;
   return svcipcSubmitCommandAndWait(&cmd->header);
}

SVCIPC_tError SVCIPC_asyncSubscribeOwnerChanged(SVCIPC_tConnection conn, SVCIPC_tConstStr busName,
      SVCIPC_tNameOwnerChangedCallback onOwnerChanged, SVCIPC_tSubscriptionCallback onSubscription,
      SVCIPC_tUserToken token)
{
   if (!onOwnerChanged)
      return SVCIPC_ERROR_BAD_ARGS;

   SubscribeCommand *cmd = newOwnerCommand(conn, busName, onOwnerChanged, token);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->onSubscription = onSubscription;
   cmd->token = token;
   return svcipcSubmitCommand(&cmd->header);
}
