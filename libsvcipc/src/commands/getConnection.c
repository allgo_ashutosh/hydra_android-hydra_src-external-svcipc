/*
 * getConnection.c
 *
 *  Created on: Apr 3, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "dispatcher.h"

struct command
{
   Command header;
   SVCIPC_tConnType connType;
   SVCIPC_tBool openPrivate;
   SVCIPC_tConnectionCallback onConnect;
   SVCIPC_tUserToken token;
   SVCIPC_tConnection *connPtr;
};

static void execute(struct command *cmd)
{
   TRACE_INFO("execute getConnection");
   SVCIPC_tError error = SVCIPC_ERROR_DBUS;
   DBusError dbusError;
   dbus_error_init(&dbusError);

   Connection *conn = svcipcGetConnection(cmd->connType, cmd->openPrivate, &dbusError);
   if (conn)
   {
      error = SVCIPC_ERROR_NONE;
   }

   if (cmd->onConnect)
   {
      SVCIPC_tCallbackStatus status =
      { error, SVCIPC_ERR_NAME_OK, NULL };
      if (!conn)
      {
         status.errName = dbusError.name;
         status.errMsg = dbusError.message;
      }
      cmd->onConnect(&status, conn, cmd->token);
   }
   else
   {
      *cmd->connPtr = conn;
      svcipcPostResult(&cmd->header, error);
   }

   dbus_error_free(&dbusError);
   free(cmd);
}

static int validConnectionType(SVCIPC_tConnType connType)
{
   // TODO: allow other bus types (although they won't work, why bother?)
   // For now just ignore it (we do in getConnection)
   return connType == SVCIPC_CONNECTION_SESSION || connType == SVCIPC_CONNECTION_SYSTEM || connType == SVCIPC_CONNECTION_SYSTEM;
}

SVCIPC_tError SVCIPC_getConnection(SVCIPC_tConnType connType, SVCIPC_tBool openPrivate, SVCIPC_tConnection* connPtr)
{
   if (!(validConnectionType(connType) && connPtr))
      return SVCIPC_ERROR_BAD_ARGS;

   struct command *cmd = svcipcCommand(sizeof(struct command), execute, 0);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->connType = connType;
   cmd->openPrivate = openPrivate;
   cmd->connPtr = connPtr;

   return svcipcSubmitCommandAndWait(&cmd->header);
}

SVCIPC_tError SVCIPC_asyncGetConnection(SVCIPC_tConnType connType, SVCIPC_tBool openPrivate, SVCIPC_tConnectionCallback onConnect,
      SVCIPC_tUserToken token)
{
   if (!(validConnectionType(connType) && onConnect))
      return SVCIPC_ERROR_BAD_ARGS;

   struct command *cmd = svcipcCommand(sizeof(struct command), execute, 0);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->connType = connType;
   cmd->openPrivate = openPrivate;
   cmd->onConnect = onConnect;
   cmd->token = token;

   return svcipcSubmitCommand(&cmd->header);
}
