/*
 * unsubscribe.c
 *
 *  Created on: Apr 4, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "dispatcher.h"
#include "subscription.h"

typedef struct
{
   Command header;
   SVCIPC_tStatusCallback onStatus;
   SVCIPC_tUserToken token;
   Subscription *subscription;
} UnsubscribeCommand;

////////////////////////////////////////////////////////////
// Dispatch command results

static void dispatchResult(UnsubscribeCommand *cmd, int errCode, const char *errName, const char *errMsg)
{
   if (cmd->header.sem)
   {
      svcipcPostResult(&cmd->header, errCode);
   }
   else if (cmd->onStatus)
   {
      SVCIPC_tCallbackStatus status =
      { errCode, errName, errMsg };
      cmd->onStatus(&status, cmd->token);
   }

   // remove it from the connection regardless of whether we were successful removing it from dbus
   svcipcRemoveSubscription(cmd->subscription);
   free(cmd);
}

////////////////////////////////////////////////////////////
// Process pending command

static void onPendingCallNotify(DBusPendingCall *call, void *data)
{
   UnsubscribeCommand *cmd = data;

   DBusMessage *reply = dbus_pending_call_steal_reply(call);
   if (reply)
   {
      int replyType = dbus_message_get_type(reply);

      if (replyType == DBUS_MESSAGE_TYPE_METHOD_RETURN)
      {
         // give the subscription to the connection
         dispatchResult(cmd, SVCIPC_ERROR_NONE, 0, 0);
      }
      else if (replyType == DBUS_MESSAGE_TYPE_ERROR)
      {
         const char *errName = dbus_message_get_error_name(reply);
         const char *errMsg = 0;
         if (!dbus_message_get_args(reply, 0, DBUS_TYPE_STRING, &errMsg, DBUS_TYPE_INVALID))
         {
            TRACE_WARN("onPendingCallNotify: Failed to extract error message");
         }
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, errName, errMsg);
      }
      else
      {
         TRACE_WARN("onPendingCallNotify: Received unexpected D-Bus message type (%d)", replyType);
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, SVCIPC_ERR_NAME_DBUS, "Unknown message reply type");
      }

      // Free the reply message
      dbus_message_unref(reply);
   }
   else
   {
      dispatchResult(cmd, SVCIPC_ERROR_INTERNAL, SVCIPC_ERR_NAME_INTERNAL, "Failed to retrieve reply message");
   }
}

////////////////////////////////////////////////////////////

static DBusMessage *buildMessage(UnsubscribeCommand *cmd)
{
   DBusMessage* msg = dbus_message_new_method_call(DBUS_SERVICE_DBUS, DBUS_PATH_DBUS, DBUS_INTERFACE_DBUS, "RemoveMatch");
   if (msg)
   {
      const char *rule = cmd->subscription->rule;
      dbus_message_set_no_reply(msg, FALSE);
      if (!dbus_message_append_args(msg, DBUS_TYPE_STRING, &rule, DBUS_TYPE_INVALID))
      {
         dbus_message_unref(msg);
         return 0;
      }
   }
   return msg;
}

static void execute(UnsubscribeCommand *cmd)
{
   TRACE_INFO("execute unsubscribe sub=%p", cmd->subscription);

   if (!svcipcSubscriptionExists(cmd->subscription))
   {
      dispatchResult(cmd, SVCIPC_MAKE_ERROR(SVCIPC_ERROR_LEVEL_ERROR, SVCIPC_DOMAIN_IPC_LIB, SVCIPC_ERR_NOT_FOUND),
            SVCIPC_ERR_NAME_NOT_FOUND, "Subscription does not exist");
      return;
   }

   Connection *conn = cmd->subscription->conn;
   if (!svcipcIsConnected(conn))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NOT_CONNECTED, SVCIPC_ERR_NAME_NOT_CONNECTED, "Not connected");
      return;
   }

   DBusMessage* msg = buildMessage(cmd);
   if (!msg)
   {
      dispatchResult(cmd, SVCIPC_ERROR_NO_MEMORY, SVCIPC_ERR_NAME_NO_MEMORY, "Out of memory");
      return;
   }

   DBusConnection *dbusConn = svipcGetDBusConnection(conn);
   DBusPendingCall *call;
   if (!(dbus_connection_send_with_reply(dbusConn, msg, &call, -1) && dbus_pending_call_set_notify(call, onPendingCallNotify, cmd, 0)))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NO_MEMORY, SVCIPC_ERR_NAME_NO_MEMORY, "Out of memory");
   }

   dbus_message_unref(msg);
}

////////////////////////////////////////////////////////////

SVCIPC_tError SVCIPC_unsubscribe(SVCIPC_tSigSubHnd subHnd)
{
   if (!subHnd)
      return SVCIPC_ERROR_BAD_ARGS;

   UnsubscribeCommand *cmd = svcipcCommand(sizeof(UnsubscribeCommand), execute, 0);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->subscription = subHnd;

   return svcipcSubmitCommandAndWait(&cmd->header);
}

SVCIPC_tError SVCIPC_asyncUnsubscribe(SVCIPC_tSigSubHnd subHnd, SVCIPC_tStatusCallback onStatus, SVCIPC_tUserToken token)
{
   if (!subHnd)
      return SVCIPC_ERROR_BAD_ARGS;

   UnsubscribeCommand *cmd = svcipcCommand(sizeof(UnsubscribeCommand), execute, 0);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->subscription = subHnd;
   cmd->onStatus = onStatus;
   cmd->token = token;

   return svcipcSubmitCommand(&cmd->header);
}
