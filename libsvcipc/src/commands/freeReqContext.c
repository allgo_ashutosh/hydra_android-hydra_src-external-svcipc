/*
 * freeReqContext.c
 *
 *  Created on: Apr 12, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "context.h"

typedef struct
{
   Command header;
   Context *context;
} FreeContextCommand;

static void execute(FreeContextCommand *cmd)
{
   TRACE_INFO("execute freeContext %p", cmd->context);
   svcipcFreeContext(cmd->context);
   free(cmd);
}

void SVCIPC_freeReqContext(SVCIPC_tReqContext context)
{
   if (context)
   {
      FreeContextCommand *cmd = svcipcCommand(sizeof(FreeContextCommand), execute, 0);
      cmd->context = context;
      svcipcSubmitCommand(&cmd->header);
   }
}
