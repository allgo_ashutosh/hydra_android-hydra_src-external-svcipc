/*
 * closeConnection.c
 *
 *  Created on: Apr 3, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "dispatcher.h"

static void execute(Command *cmd, Connection *conn)
{
   TRACE_INFO("execute closeConnection conn=%p", conn);
   SVCIPC_tError error = SVCIPC_ERROR_NONE;

   if (!svcipcCloseConnection(conn))
   {
      error = SVCIPC_ERROR_BAD_ARGS; // user passed in an invalid connection
   }

   if (cmd->sem)
      svcipcPostResult(cmd, error);

   free(cmd);
}

SVCIPC_tError SVCIPC_closeConnection(SVCIPC_tConnection conn)
{
   Command *cmd = svcipcCommand(sizeof(Command), execute, conn);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   return svcipcSubmitCommandAndWait(cmd);
}

SVCIPC_tError SVCIPC_asyncCloseConnection(SVCIPC_tConnection conn)
{
   Command *cmd = svcipcCommand(sizeof(Command), execute, conn);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   return svcipcSubmitCommand(cmd);
}
