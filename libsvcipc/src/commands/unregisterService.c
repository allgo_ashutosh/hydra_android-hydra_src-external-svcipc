/*
 * unregisterService.c
 *
 *  Created on: Apr 10, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "registration.h"

typedef struct
{
   Command header;
   Registration *reg;
   SVCIPC_tStatusCallback onStatus;
   SVCIPC_tUserToken token;

} UnregisterServiceCommand;

static void dispatchResult(UnregisterServiceCommand *cmd, int errCode, const char *errName, const char *errMsg)
{
   if (cmd->header.sem)
   {
      svcipcPostResult(&cmd->header, errCode);
   }
   else if (cmd->onStatus)
   {
      SVCIPC_tCallbackStatus status =
      { errCode, errName, errMsg };
      cmd->onStatus(&status, cmd->token);
   }

   // remove it from the connection regardless of whether we were successful removing it from dbus
   svcipcRemoveRegistration(cmd->reg);
   free(cmd);
}

////////////////////////////////////////////////////////////
// Process pending command

static void onPendingCallNotify(DBusPendingCall *call, void *data)
{
   UnregisterServiceCommand *cmd = data;

   DBusMessage *reply = dbus_pending_call_steal_reply(call);
   if (reply)
   {
      int replyType = dbus_message_get_type(reply);

      if (replyType == DBUS_MESSAGE_TYPE_METHOD_RETURN)
      {
         dbus_uint32_t result = 0;
         if (!dbus_message_get_args(reply, 0, DBUS_TYPE_UINT32, &result, DBUS_TYPE_INVALID) || result
               != DBUS_RELEASE_NAME_REPLY_RELEASED)
         {
            // Let's just log a warning ...
            TRACE_WARN("OnPendingCallNotify: Failed to release bus name");
         }
         TRACE_INFO("ReleaseName returned %u", result);
         dispatchResult(cmd, SVCIPC_ERROR_NONE, 0, 0);
      }
      else if (replyType == DBUS_MESSAGE_TYPE_ERROR)
      {
         const char *errName = dbus_message_get_error_name(reply);
         const char *errMsg = 0;
         if (!dbus_message_get_args(reply, 0, DBUS_TYPE_STRING, &errMsg, DBUS_TYPE_INVALID))
         {
            TRACE_WARN("onPendingCallNotify: Failed to extract error message");
         }
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, errName, errMsg);
      }
      else
      {
         TRACE_WARN("onPendingCallNotify: Received unexpected D-Bus message type (%d)", replyType);
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, SVCIPC_ERR_NAME_DBUS, "Unknown message reply type");
      }

      // Free the reply message
      dbus_message_unref(reply);
   }
   else
   {
      dispatchResult(cmd, SVCIPC_ERROR_INTERNAL, SVCIPC_ERR_NAME_INTERNAL, "Failed to retrieve reply message");
   }
}

////////////////////////////////////////////////////////////

static DBusMessage *buildMessage(const char *busName)
{
   DBusMessage* msg = dbus_message_new_method_call(DBUS_SERVICE_DBUS, DBUS_PATH_DBUS, DBUS_INTERFACE_DBUS,
         "ReleaseName");
   if (msg)
   {
      dbus_message_set_no_reply(msg, FALSE);
      if (!dbus_message_append_args(msg, DBUS_TYPE_STRING, &busName, DBUS_TYPE_INVALID))
      {
         dbus_message_unref(msg);
         return 0;
      }
   }
   return msg;
}

static void execute(UnregisterServiceCommand *cmd, Connection *conn)
{
   TRACE_INFO("execute unregisterService reg=%p", cmd->reg);

   if (!svcipcRegistrationExists(cmd->reg))
   {
      dispatchResult(cmd, SVCIPC_ERROR_BAD_ARGS, SVCIPC_ERR_NAME_BAD_ARGS, "Registration does not exist");
      return;
   }

   if (!svcipcIsConnected(cmd->reg->conn))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NOT_CONNECTED, SVCIPC_ERR_NAME_NOT_CONNECTED, "Not connected");
      return;
   }

   DBusMessage* msg = buildMessage(cmd->reg->busName);
   if (!msg)
   {
      dispatchResult(cmd, SVCIPC_ERROR_NO_MEMORY, SVCIPC_ERR_NAME_NO_MEMORY, "Out of memory");
      return;
   }

   DBusConnection *dbusConn = svipcGetDBusConnection(cmd->reg->conn);
   DBusPendingCall *call;
   if (!(dbus_connection_send_with_reply(dbusConn, msg, &call, -1) && dbus_pending_call_set_notify(call,
         onPendingCallNotify, cmd, 0)))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NO_MEMORY, SVCIPC_ERR_NAME_NO_MEMORY, "Out of memory");
   }

   dbus_message_unref(msg);
}

////////////////////////////////////////////////////////////

SVCIPC_tError SVCIPC_unregisterService(SVCIPC_tSvcRegHnd regHnd)
{
   if (!regHnd)
   {
      TRACE_ERROR("SVCIPC_unregisterService regHnd=NULL");
      return SVCIPC_ERROR_BAD_ARGS;
   }

   UnregisterServiceCommand *cmd = svcipcCommand(sizeof(UnregisterServiceCommand), execute, 0);
   if (!cmd)
   {
      TRACE_ERROR("SVCIPC_unregisterService out of memory");
      return SVCIPC_ERROR_NO_MEMORY;
   }

   cmd->reg = regHnd;
   return svcipcSubmitCommandAndWait(&cmd->header);
}

SVCIPC_tError SVCIPC_asyncUnregisterService(SVCIPC_tSvcRegHnd regHnd, SVCIPC_tStatusCallback onStatus,
      SVCIPC_tUserToken token)
{
   if (!regHnd)
   {
      TRACE_ERROR("SVCIPC_asyncUnregisterService regHnd=NULL");
      return SVCIPC_ERROR_BAD_ARGS;
   }

   UnregisterServiceCommand *cmd = svcipcCommand(sizeof(UnregisterServiceCommand), execute, 0);
   if (!cmd)
   {
      TRACE_ERROR("SVCIPC_unregisterService out of memory");
      return SVCIPC_ERROR_NO_MEMORY;
   }

   cmd->reg = regHnd;
   cmd->onStatus = onStatus;
   cmd->token = token;
   return svcipcSubmitCommand(&cmd->header);
}
