/*
 * registerService.c
 *
 *  Created on: Apr 7, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "dispatcher.h"
#include "registration.h"

typedef struct
{
   Command header;
   char *busName;
   char *objPath;
   SVCIPC_tUInt32 flag;
   SVCIPC_tRegistrationCallback onRegister;
   SVCIPC_tUserToken token;
   SVCIPC_tSvcRegHnd *regHnd;
   Registration *reg;
} RegisterServiceCommand;


static void freeCommand(RegisterServiceCommand *cmd)
{
   svcipcFreeRegistration(cmd->reg);
   free(cmd->busName);
   free(cmd->objPath);
   free(cmd);
}

////////////////////////////////////////////////////////////
// Dispatch command results

static void dispatchResult(RegisterServiceCommand *cmd, int errCode, const char *errName, const char *errMsg,
      Registration *reg)
{
   if (cmd->header.sem)
   {
      *cmd->regHnd = reg;
      svcipcPostResult(&cmd->header, errCode);
   }
   else if (cmd->onRegister)
   {
      SVCIPC_tCallbackStatus status =
      { errCode, errName, errMsg };
      cmd->onRegister(&status, reg, cmd->token);
   }

   freeCommand(cmd);
}

////////////////////////////////////////////////////////////
// Process pending command

static void onPendingCallNotify(DBusPendingCall *call, void *data)
{
   RegisterServiceCommand *cmd = data;

   DBusMessage *reply = dbus_pending_call_steal_reply(call);
   if (reply)
   {
      int replyType = dbus_message_get_type(reply);

      if (replyType == DBUS_MESSAGE_TYPE_METHOD_RETURN)
      {
         dbus_uint32_t result;
         if (dbus_message_get_args(reply, 0, DBUS_TYPE_UINT32, &result, DBUS_TYPE_INVALID))
         {
            // If we acquired the bus name (or already own it) then register the service with the connection
            if (result == DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER || result == DBUS_REQUEST_NAME_REPLY_ALREADY_OWNER)
            {
               // give ownership of the registration to the connection
               Registration *reg = cmd->reg;
               svcipcAddRegistration(reg);
               cmd->reg = 0;

               dispatchResult(cmd, SVCIPC_ERROR_NONE, 0, 0, reg);
            }
            else
            {
               TRACE_WARN("RequestName returned %u", result);
               dispatchResult(cmd, SVCIPC_ERROR_DBUS, SVCIPC_ERR_NAME_DBUS, "Name is already owned", 0);
            }
         }
         else
         {
            TRACE_WARN("registerService: Failed to extract result from reply");
            dispatchResult(cmd, SVCIPC_ERROR_DBUS, SVCIPC_ERR_NAME_DBUS, "Failed to extract result from reply", 0);
         }
      }
      else if (replyType == DBUS_MESSAGE_TYPE_ERROR)
      {
         const char *errName = dbus_message_get_error_name(reply);
         const char *errMsg = 0;
         if (!dbus_message_get_args(reply, 0, DBUS_TYPE_STRING, &errMsg, DBUS_TYPE_INVALID))
         {
            TRACE_WARN("registerService: Failed to extract error message");
         }
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, errName, errMsg, 0);
      }
      else
      {
         TRACE_WARN("registerService: Received unexpected D-Bus message type (%d)", replyType);
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, SVCIPC_ERR_NAME_DBUS, "Unknown message reply type", 0);
      }

      // Free the reply message
      dbus_message_unref(reply);
   }
   else
   {
      dispatchResult(cmd, SVCIPC_ERROR_INTERNAL, SVCIPC_ERR_NAME_INTERNAL, "Failed to retrieve reply message", 0);
   }
}

////////////////////////////////////////////////////////////
// Execute command

static DBusMessage *buildMessage(RegisterServiceCommand *cmd)
{
   DBusMessage* msg = dbus_message_new_method_call(DBUS_SERVICE_DBUS, DBUS_PATH_DBUS, DBUS_INTERFACE_DBUS,
         "RequestName");
   if (msg)
   {
      const char *busName = cmd->busName;
      const uint32_t flags = DBUS_NAME_FLAG_DO_NOT_QUEUE | DBUS_NAME_FLAG_REPLACE_EXISTING;
      dbus_message_set_no_reply(msg, FALSE);
      if (!dbus_message_append_args(msg, DBUS_TYPE_STRING, &busName, DBUS_TYPE_UINT32, &flags, DBUS_TYPE_INVALID))
      {
         dbus_message_unref(msg);
         msg = 0;
      }
   }
   return msg;
}

static void execute(RegisterServiceCommand *cmd, Connection *conn)
{
   TRACE_INFO("execute registerService conn=%p", conn);

   if (!svcipcIsConnected(conn))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NOT_CONNECTED, SVCIPC_ERR_NAME_NOT_CONNECTED, "Not connected", 0);
      return;
   }

   DBusMessage* msg = buildMessage(cmd);
   if (!msg)
   {
      dispatchResult(cmd, SVCIPC_ERROR_NO_MEMORY, SVCIPC_ERR_NAME_NO_MEMORY, "Out of memory", 0);
      return;
   }

   DBusConnection *dbusConn = svipcGetDBusConnection(conn);
   DBusPendingCall *call;
   if (!(dbus_connection_send_with_reply(dbusConn, msg, &call, -1) && dbus_pending_call_set_notify(call,
         onPendingCallNotify, cmd, 0)))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NO_MEMORY, SVCIPC_ERR_NAME_NO_MEMORY, "Out of memory", 0);
   }

   dbus_message_unref(msg);
}

////////////////////////////////////////////////////////////
// Submit commands

static RegisterServiceCommand *newCommand(SVCIPC_tConnection conn, SVCIPC_tConstStr busName, SVCIPC_tConstStr objPath,
      SVCIPC_tUInt32 flag, SVCIPC_tRequestCallback onRequest, SVCIPC_tUserToken token)
{
   RegisterServiceCommand *cmd = svcipcCommand(sizeof(RegisterServiceCommand), execute, conn);
   if (cmd)
   {
      cmd->busName = strdup(busName ? busName : "");
      cmd->objPath = objPath ? strdup(objPath) : svcipcBusNameToObjPath(busName);
      cmd->flag = flag;
      if (cmd->busName && cmd->objPath)
      {
         cmd->reg = svcipcNewRegistration(conn, cmd->busName, cmd->objPath, flag, onRequest, token);
         if (cmd->reg)
         {
            return cmd;
         }
      }
      freeCommand(cmd);
   }
   return 0;
}

SVCIPC_tError SVCIPC_registerService(SVCIPC_tConnection conn, SVCIPC_tConstStr busName, SVCIPC_tConstStr objPath,
      SVCIPC_tUInt32 flag, SVCIPC_tRequestCallback onRequest, SVCIPC_tUserToken token, SVCIPC_tSvcRegHnd* regHnd)
{
   if (!regHnd)
   {
      TRACE_ERROR("SVCIPC_registerService regHnd=NULL");
      return SVCIPC_ERROR_BAD_ARGS;
   }

   RegisterServiceCommand *cmd = newCommand(conn, busName, objPath, flag, onRequest, token);
   if (!cmd)
   {
      TRACE_ERROR("SVCIPC_registerService out of memory");
      return SVCIPC_ERROR_NO_MEMORY;
   }

   cmd->regHnd = regHnd;
   return svcipcSubmitCommandAndWait(&cmd->header);
}

SVCIPC_tError SVCIPC_asyncRegisterService(SVCIPC_tConnection conn, SVCIPC_tConstStr busName, SVCIPC_tConstStr objPath,
      SVCIPC_tUInt32 flag, SVCIPC_tRequestCallback onRequest, SVCIPC_tRegistrationCallback onRegister,
      SVCIPC_tUserToken token)
{
   RegisterServiceCommand *cmd = newCommand(conn, busName, objPath, flag, onRequest, token);
   if (!cmd)
   {
      TRACE_ERROR("SVCIPC_registerService out of memory");
      return SVCIPC_ERROR_NO_MEMORY;
   }

   cmd->onRegister = onRegister;
   cmd->token = token;
   return svcipcSubmitCommand(&cmd->header);
}
