/*
 * openConnection.c
 *
 *  Created on: Apr 3, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "dispatcher.h"

#if 0
struct openConnectionCommand
{
   Command header;
   SVCIPC_tConstStr address;
   SVCIPC_tBool openPrivate;
   SVCIPC_tConnection* connPtr;
};

SVCIPC_tError SVCIPC_openConnection(SVCIPC_tConstStr address, SVCIPC_tBool openPrivate, SVCIPC_tConnection* conn)
{
   return SVCIPC_ERROR_NOT_SUPPORTED;
}

SVCIPC_tError SVCIPC_asyncOpenConnection(SVCIPC_tConstStr address, SVCIPC_tBool openPrivate, SVCIPC_tConnectionCallback onConnect,
      SVCIPC_tUserToken token)
{
   return SVCIPC_ERROR_NOT_SUPPORTED;
}
#endif
