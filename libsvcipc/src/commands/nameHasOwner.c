/*
 * nameHasOwner.c
 *
 *  Created on: Apr 2, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "dispatcher.h"

struct nameHasOwnerCommand
{
   Command header;
   char *busName;
   SVCIPC_tNameHasOwnerCallback callback;
   SVCIPC_tUserToken token;
   SVCIPC_tBool *hasOwner;
};

////////////////////////////////////////////////////////////
// Dispatch result

static void dispatchResult(struct nameHasOwnerCommand *cmd, int errCode, const char *errName, const char *errMsg, int hasOwner)
{
   if (cmd->callback)
   {
      SVCIPC_tCallbackStatus status =
      { errCode, errName, errMsg };
      cmd->callback(&status, cmd->busName, hasOwner, cmd->token);
   }
   else
   {
      *cmd->hasOwner = hasOwner;
      svcipcPostResult(&cmd->header, errCode);
   }

   free(cmd->busName);
   free(cmd);
}

////////////////////////////////////////////////////////////
// Process pending command

static void onPendingCallNotify(DBusPendingCall *call, void *data)
{
   struct nameHasOwnerCommand *cmd = data;

   DBusMessage *reply = dbus_pending_call_steal_reply(call);
   if (reply)
   {
      int replyType = dbus_message_get_type(reply);

      if (replyType == DBUS_MESSAGE_TYPE_METHOD_RETURN)
      {
         dbus_bool_t hasOwner = FALSE;
         if (!dbus_message_get_args(reply, 0, DBUS_TYPE_BOOLEAN, &hasOwner, DBUS_TYPE_INVALID))
         {
            TRACE_WARN("onPendingCallNotify: Failed to extract results");
         }
         dispatchResult(cmd, SVCIPC_ERROR_NONE, 0, 0, hasOwner);
      }
      else if (replyType == DBUS_MESSAGE_TYPE_ERROR)
      {
         const char *errName = dbus_message_get_error_name(reply);
         const char *errMsg = 0;
         if (!dbus_message_get_args(reply, 0, DBUS_TYPE_STRING, &errMsg, DBUS_TYPE_INVALID))
         {
            TRACE_WARN("onPendingCallNotify: Failed to extract error message");
         }
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, errName, errMsg, FALSE);
      }
      else
      {
         TRACE_WARN("onPendingCallNotify: Received unexpected D-Bus message type (%d)", replyType);
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, SVCIPC_ERR_NAME_DBUS, "Unknown message reply type", FALSE);
      }

      // Free the reply message
      dbus_message_unref(reply);
   }
   else
   {
      dispatchResult(cmd, SVCIPC_ERROR_INTERNAL, SVCIPC_ERR_NAME_INTERNAL, "Failed to retrieve reply message", FALSE);
   }
}

////////////////////////////////////////////////////////////
// Execute command

DBusMessage *nameHasOwnerMessage(char **busName)
{
   DBusMessage* msg = dbus_message_new_method_call(DBUS_SERVICE_DBUS, DBUS_PATH_DBUS, DBUS_INTERFACE_DBUS, "NameHasOwner");
   if (msg)
   {
      dbus_message_append_args(msg, DBUS_TYPE_STRING, busName, DBUS_TYPE_INVALID);
      dbus_message_set_no_reply(msg, FALSE);
   }
   return msg;
}

static void execute(struct nameHasOwnerCommand *cmd, Connection *conn)
{
   TRACE_INFO("execute nameHasOwner conn=%p", conn);

   if (!svcipcIsConnected(conn))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NOT_CONNECTED, SVCIPC_ERR_NAME_NOT_CONNECTED, "Not connected", FALSE);
      return;
   }

   DBusConnection *dbusConn = svipcGetDBusConnection(conn);
   DBusMessage* msg = nameHasOwnerMessage(&cmd->busName);
   DBusPendingCall *call;
   if (!msg || !dbus_connection_send_with_reply(dbusConn, msg, &call, -1) || !dbus_pending_call_set_notify(call, onPendingCallNotify, cmd,
         0))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NO_MEMORY, SVCIPC_ERR_NAME_NO_MEMORY, "Out of memory", FALSE);
      return;
   }

   // Free the request message (finally)
   dbus_message_unref(msg);
}

////////////////////////////////////////////////////////////
// Invoke method

SVCIPC_tError SVCIPC_asyncNameHasOwner(SVCIPC_tConnection conn, SVCIPC_tConstStr busName, SVCIPC_tNameHasOwnerCallback onHasOwner,
      SVCIPC_tUserToken token)
{
   if (!(busName && onHasOwner))
      return SVCIPC_ERROR_BAD_ARGS;

   struct nameHasOwnerCommand *cmd = svcipcCommand(sizeof(struct nameHasOwnerCommand), execute, conn);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->busName = strdup(busName);
   cmd->callback = onHasOwner;
   cmd->token = token;

   return svcipcSubmitCommand(&cmd->header);
}

SVCIPC_tError SVCIPC_nameHasOwner(SVCIPC_tConnection conn, SVCIPC_tConstStr busName, SVCIPC_tBool* hasOwner)
{
   if (!(busName && hasOwner))
      return SVCIPC_ERROR_BAD_ARGS;

   struct nameHasOwnerCommand *cmd = svcipcCommand(sizeof(struct nameHasOwnerCommand), execute, conn);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->busName = strdup(busName);
   cmd->hasOwner = hasOwner;

   return svcipcSubmitCommandAndWait(&cmd->header);
}
