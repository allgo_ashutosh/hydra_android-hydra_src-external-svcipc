/*
 * emit.c
 *
 *  Created on: Apr 10, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "registration.h"

typedef struct
{
   Command header;
   Registration *reg;
   char *sigName;
   char *sigArgs;
   SVCIPC_tStatusCallback onStatus;
   SVCIPC_tUserToken token;

} EmitCommand;

////////////////////////////////////////////////////////////

static void freeCommand(EmitCommand *cmd)
{
   if (cmd)
   {
      free(cmd->sigName);
      free(cmd->sigArgs);
      free(cmd);
   }
}

////////////////////////////////////////////////////////////
// Dispatch result

static void dispatchResult(EmitCommand *cmd, int errCode, const char *errName, const char *errMsg)
{
   if (cmd->header.sem)
   {
      svcipcPostResult(&cmd->header, errCode);
   }
   else if (cmd->onStatus)
   {
      SVCIPC_tCallbackStatus status =
      { errCode, errName, errMsg };
      cmd->onStatus(&status, cmd->token);
   }

   freeCommand(cmd);
}

////////////////////////////////////////////////////////////
// Execute command

static void execute(EmitCommand *cmd)
{
   Registration *reg = cmd->reg;
   TRACE_INFO("execute emit reg=%p sig=%s args=%s", reg, cmd->sigName, cmd->sigArgs);

   if (!svcipcRegistrationExists(reg))
   {
      dispatchResult(cmd, SVCIPC_MAKE_ERROR(SVCIPC_ERROR_LEVEL_ERROR, SVCIPC_DOMAIN_IPC_LIB, SVCIPC_ERR_NOT_FOUND),
            SVCIPC_ERR_NAME_NOT_FOUND, "Registration does not exist");
      return;
   }

   if (!svcipcIsConnected(reg->conn))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NOT_CONNECTED, SVCIPC_ERR_NAME_NOT_CONNECTED, "Not connected");
      return;
   }

   DBusMessage *signal = dbus_message_new_signal(reg->objPath, SVCIPC_INTERFACE_NAME, SVCIPC_INTERFACE_SIGNAL_NAME);
   if (!signal)
   {
      dispatchResult(cmd, SVCIPC_ERROR_NO_MEMORY, SVCIPC_ERR_NAME_NO_MEMORY, "Cannot allocate signal");
      return;
   }

   dbus_uint32_t serial;
   if (dbus_message_append_args(signal, DBUS_TYPE_STRING, &cmd->sigName, DBUS_TYPE_STRING, &cmd->sigArgs,
         DBUS_TYPE_INVALID) && dbus_connection_send(svipcGetDBusConnection(reg->conn), signal, &serial))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NONE, SVCIPC_ERR_NAME_OK, 0);
   }
   else
   {
      dispatchResult(cmd, SVCIPC_MAKE_ERROR(SVCIPC_ERROR_LEVEL_ERROR, SVCIPC_DOMAIN_IPC_LIB, SVCIPC_ERR_CONN_SEND),
            SVCIPC_ERR_NAME_CONN_SEND, "Failed to send message over bus");
   }

   dbus_message_unref(signal);
}

////////////////////////////////////////////////////////////
// Invoke method

static EmitCommand *newCommand(Registration *reg, const char *sigName, const char *sigArgs)
{
   EmitCommand *cmd = svcipcCommand(sizeof(EmitCommand), execute, 0);
   if (cmd)
   {
      cmd->reg = reg;
      cmd->sigName = strdup(sigName ? sigName : "");
      cmd->sigArgs = strdup(sigArgs ? sigArgs : "{}");

      if (!(cmd->sigName && cmd->sigArgs))
      {
         freeCommand(cmd);
         cmd = 0;
      }
   }
   return cmd;
}

SVCIPC_tError SVCIPC_emit(SVCIPC_tSvcRegHnd regHnd, SVCIPC_tConstStr sigName, SVCIPC_tConstStr sigArgs)
{
   if (!regHnd)
   {
      TRACE_WARN("SVCIPC_emit regHnd=NULL");
      return SVCIPC_ERROR_BAD_ARGS;
   }

   EmitCommand *cmd = newCommand(regHnd, sigName, sigArgs);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   return svcipcSubmitCommandAndWait(&cmd->header);
}

SVCIPC_tError SVCIPC_asyncEmit(SVCIPC_tSvcRegHnd regHnd, SVCIPC_tConstStr sigName, SVCIPC_tConstStr sigArgs,
      SVCIPC_tStatusCallback onStatus, SVCIPC_tUserToken token)
{
   if (!regHnd)
   {
      TRACE_WARN("SVCIPC_emit regHnd=NULL");
      return SVCIPC_ERROR_BAD_ARGS;
   }

   EmitCommand *cmd = newCommand(regHnd, sigName, sigArgs);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->onStatus = onStatus;
   cmd->token = token;
   return svcipcSubmitCommand(&cmd->header);
}
