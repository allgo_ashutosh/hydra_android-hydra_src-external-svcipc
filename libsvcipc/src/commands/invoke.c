/*
 * invoke.c
 *
 *  Created on: Apr 2, 2012
 *      Author: AMckewan
 */

#include "command.h"
#include "dispatcher.h"

typedef struct
{
   Command header;
   char *busName;
   char *objPath;
   char *method;
   char *args;
   int noReplyExpected;
   SVCIPC_tUInt32 timeout;
   SVCIPC_tResultCallback onResult;
   SVCIPC_tUserToken token;
   SVCIPC_tResponse** response;
   SVCIPC_tHandle* handle;
   uint32_t serialNum;

} InvokeCommand;

static void freeCommand(InvokeCommand *cmd)
{
   if (cmd)
   {
      free(cmd->busName);
      free(cmd->objPath);
      free(cmd->method);
      free(cmd->args);
      free(cmd);
   }
}

////////////////////////////////////////////////////////////
// Dispatch result

static void dispatchResult(InvokeCommand *cmd, int errCode, const char *errName, const char *errMsg, const char *result)
{
   if (cmd->onResult)
   {
      SVCIPC_tCallbackStatus status =
      { errCode, errName, errMsg };
      cmd->onResult(&status, result, cmd->token);
   }
   else
   {
      SVCIPC_tResponse *response = *cmd->response;
      response->result = svcipcStrdup(result);
      response->status.errCode = errCode;
      response->status.errName = svcipcStrdup(errName);
      response->status.errMsg = svcipcStrdup(errMsg);

      svcipcPostResult(&cmd->header, errCode);
   }

   freeCommand(cmd);
}

static void dispatchOutOfMemory(InvokeCommand *cmd)
{
   return dispatchResult(cmd, SVCIPC_ERROR_NOT_CONNECTED, SVCIPC_ERR_NAME_NOT_CONNECTED, "Not connected", 0);
}

////////////////////////////////////////////////////////////
// Process pending command

static void onPendingCallNotify(DBusPendingCall *call, void *data)
{
   InvokeCommand *cmd = data;

   DBusMessage *reply = dbus_pending_call_steal_reply(call);
   if (reply)
   {
      int replyType = dbus_message_get_type(reply);

      if (replyType == DBUS_MESSAGE_TYPE_METHOD_RETURN)
      {
         const char *result = 0;
         if (!dbus_message_get_args(reply, 0, DBUS_TYPE_STRING, &result, DBUS_TYPE_INVALID))
         {
            TRACE_WARN("onPendingCallNotify: Failed to extract method result");
         }
         dispatchResult(cmd, SVCIPC_ERROR_NONE, 0, 0, result);
      }
      else if (replyType == DBUS_MESSAGE_TYPE_ERROR)
      {
         const char *errName = dbus_message_get_error_name(reply);
         const char *errMsg = 0;
         if (!dbus_message_get_args(reply, 0, DBUS_TYPE_STRING, &errMsg, DBUS_TYPE_INVALID))
         {
            TRACE_WARN("onPendingCallNotify: Failed to extract error message");
         }
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, errName, errMsg, 0);
      }
      else
      {
         TRACE_WARN("onPendingCallNotify: Received unexpected D-Bus message type (%d)", replyType);
         dispatchResult(cmd, SVCIPC_ERROR_DBUS, SVCIPC_ERR_NAME_DBUS, "Unknown message reply type", 0);
      }

      // Free the reply message
      dbus_message_unref(reply);
   }
   else
   {
      dispatchResult(cmd, SVCIPC_ERROR_INTERNAL, SVCIPC_ERR_NAME_INTERNAL, "Failed to retrieve reply message", 0);
   }
}

////////////////////////////////////////////////////////////
// Execute command


static void executeInvoke(InvokeCommand *cmd, Connection *conn)
{
   TRACE_INFO("execute invoke conn=%p", conn);
   if (!svcipcIsConnected(conn))
   {
      dispatchResult(cmd, SVCIPC_ERROR_NOT_CONNECTED, SVCIPC_ERR_NAME_NOT_CONNECTED, "Not connected", 0);
      return;
   }

   // build invoke message
   DBusMessage *msg = dbus_message_new_method_call(cmd->busName, cmd->objPath, SVCIPC_INTERFACE_NAME,
         SVCIPC_INTERFACE_METHOD_NAME);
   if (!msg)
   {
      dispatchOutOfMemory(cmd);
      return;
   }
   dbus_message_append_args(msg, DBUS_TYPE_STRING, &cmd->method, DBUS_TYPE_STRING, &cmd->args, DBUS_TYPE_INVALID);
   dbus_message_set_no_reply(msg, cmd->noReplyExpected);

   // send message
   DBusConnection *dbusConn = svipcGetDBusConnection(conn);
   if (cmd->noReplyExpected)
   {
      if (!dbus_connection_send(dbusConn, msg, &cmd->serialNum))
      {
         dispatchOutOfMemory(cmd);
         return;
      }
   }
   else
   {
      DBusPendingCall *call;
      if (!(dbus_connection_send_with_reply(dbusConn, msg, &call, cmd->timeout) && dbus_pending_call_set_notify(call,
            onPendingCallNotify, cmd, 0)))
      {
         dispatchOutOfMemory(cmd);
         return;
      }
   }

   // free the request message
   dbus_message_unref(msg);
}

////////////////////////////////////////////////////////////
// Invoke method

static InvokeCommand *newCommand(SVCIPC_tConnection conn, SVCIPC_tConstStr busName, SVCIPC_tConstStr objPath,
      SVCIPC_tConstStr method, SVCIPC_tConstStr args, SVCIPC_tUInt32 msecTimeout)
{
   InvokeCommand *cmd = svcipcCommand(sizeof(InvokeCommand), executeInvoke, conn);
   if (cmd)
   {
      cmd->busName = strdup(busName);
      cmd->objPath = objPath ? strdup(objPath) : svcipcBusNameToObjPath(busName);
      cmd->method = strdup(method);
      cmd->args = strdup(args ? args : "{}");
      cmd->timeout = msecTimeout;
      if (cmd->busName && cmd->objPath && cmd->method && cmd->args)
         return cmd;
      freeCommand(cmd);
   }
   return 0;
}

SVCIPC_tError SVCIPC_invoke(SVCIPC_tConnection conn, SVCIPC_tConstStr busName, SVCIPC_tConstStr objPath,
      SVCIPC_tConstStr method, SVCIPC_tConstStr args, SVCIPC_tUInt32 msecTimeout, SVCIPC_tResponse** response)
{
   if (!(busName && method && response))
      return SVCIPC_ERROR_BAD_ARGS;

   InvokeCommand *cmd = newCommand(conn, busName, objPath, method, args, msecTimeout);
   if (!(cmd && (*response = calloc(1, sizeof(SVCIPC_tResponse)))))
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->noReplyExpected = FALSE;
   cmd->response = response;

   return svcipcSubmitCommandAndWait(&cmd->header);
}

SVCIPC_tError SVCIPC_asyncInvoke(SVCIPC_tConnection conn, SVCIPC_tConstStr busName, SVCIPC_tConstStr objPath,
      SVCIPC_tConstStr method, SVCIPC_tConstStr args, SVCIPC_tBool noReplyExpected, SVCIPC_tUInt32 msecTimeout,
      SVCIPC_tResultCallback onResult, SVCIPC_tHandle* handle, SVCIPC_tUserToken token)
{
   if (!(busName && method))
      return SVCIPC_ERROR_BAD_ARGS;

   InvokeCommand *cmd = newCommand(conn, busName, objPath, method, args, msecTimeout);
   if (!cmd)
      return SVCIPC_ERROR_NO_MEMORY;

   cmd->noReplyExpected = noReplyExpected || !onResult; // reply is useless if no result callback!
   cmd->onResult = onResult;
   cmd->handle = handle;
   cmd->token = token;

   // we don't use the handle because we don't support canceling a command, but set it valid in cause the user checks it
   if (handle)
      *handle = SVCIPC_INVALID_HANDLE + 1;

   return svcipcSubmitCommand(&cmd->header);
}

void SVCIPC_freeResponse(SVCIPC_tResponse* response)
{
   if (response)
   {
      free(response->status.errName);
      free(response->status.errMsg);
      free(response->result);
      free(response);
   }
}

/*
SVCIPC_tError SVCIPC_cancel(SVCIPC_tHandle handle)
{
   return SVCIPC_ERROR_NOT_SUPPORTED;
}
*/
