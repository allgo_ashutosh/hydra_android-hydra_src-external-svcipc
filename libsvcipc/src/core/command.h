/*
 * command.h
 *
 *  Created on: Mar 30, 2012
 *      Author: AMckewan
 */

#ifndef COMMAND_H_
#define COMMAND_H_

#include <dbus/dbus.h>
#include <semaphore.h>
#include <stdlib.h>
#include <string.h>

#include "connection.h"
#include "dispatcher.h"
#include "trace.h"
#include "utils.h"

// Each command must have this structure as the first element
typedef struct Command
{
   struct Command *next;
   void (*execute)(); // for async calls
   sem_t *sem; // for blocking calls
   SVCIPC_tError *error;
   Connection *conn;
} Command;

// allocate new command
void *svcipcCommand(int size, void (*execute)(), Connection *conn);

// command queue
void svcipcCommandQueueAdd(Command *cmd);
Command *svcipcCommandQueueRemove(void);
int svcipcCommandQueueIsEmpty(void);


#endif /* COMMAND_H_ */
