/*
 * context.h
 *
 *  Created on: Apr 8, 2012
 *      Author: AMckewan
 */

#ifndef CONTEXT_H_
#define CONTEXT_H_

#include "svcipc.h"

typedef struct Context Context;
struct Connection;
struct DBusMessage;

Context *svcipcNewContext(struct Connection *conn, struct DBusMessage *msg);
void svcipcFreeContext(Context *context);
SVCIPC_tError svcipcSendReply(Context *context, const char *result);
SVCIPC_tError svcipcSendError(Context *context, const char *errName, const char *errMsg);

// this is checked at shutdown to make sure all contexts are freed
int svcipcGetAllocatedContexts(void);

#endif /* CONTEXT_H_ */
