/*
 * registration.c
 *
 *  Created on: Apr 7, 2012
 *      Author: AMckewan
 */

#include "registration.h"
#include "svcipc.h"
#include "connection.h"
#include "context.h"
#include "utils.h"
#include <process.h>
#include <stdlib.h>
#include <string.h>

Registration *svcipcNewRegistration(SVCIPC_tConnection conn, char *busName, char *objPath,
      SVCIPC_tUInt32 flag, SVCIPC_tRequestCallback onRequest, SVCIPC_tUserToken token)
{
   Registration *reg = malloc(sizeof(Registration));
   if (reg)
   {
      reg->conn = conn;
      reg->busName = strdup(busName);
      reg->objPath = strdup(objPath);
      reg->flag = flag;
      reg->onRequest = onRequest;
      reg->token = token;

      // belt and suspenders
      if (!(reg->busName && reg->objPath))
      {
         svcipcFreeRegistration(reg);
         reg = 0;
      }
   }

   return reg;
}

void svcipcFreeRegistration(Registration *reg)
{
   if (reg)
   {
      free(reg->busName);
      free(reg->objPath);
      free(reg);
   }
}

void svcipcDispatchRequest(Registration *reg, DBusMessage *msg, const char *method, const char *args,
      int timeout)
{
   if (reg->onRequest)
   {
      Context *context = svcipcNewContext(reg->conn, msg);
      if (context)
      {
         uint64_t now = svcipcGetSystemTime();
         reg->onRequest(context, method, args, dbus_message_get_no_reply(msg), reg->token);
         int elapsed = svcipcGetSystemTime() - now;
         if (elapsed > timeout)
         {
            svcipcSlog(_SLOG_WARNING, "Failed to process D-Bus onRequest within %d msec [PID=%u]", timeout, getpid());
         }
      }
   }
}
