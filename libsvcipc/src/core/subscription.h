/*
 * subscription.h
 *
 *  Created on: Apr 3, 2012
 *      Author: AMckewan
 */

#ifndef SUBSCRIPTION_H_
#define SUBSCRIPTION_H_

#include "svcipc.h"

struct Connection;

typedef struct Subscription Subscription;

struct Subscription
{
   // list node (maintained by the connection)
   Subscription *next;

   // common attributes
   char *rule;
   struct Connection *conn;
   SVCIPC_tUserToken token;

   // virtual functions
   int (*dispatchIfMatch)(Subscription *sub, DBusMessage *msg, int timeout);
   void (*free)(Subscription *sub);

   union
   {
      // nameOwnerChanged subscription
      struct
      {
         char *busName;
         SVCIPC_tNameOwnerChangedCallback onNameChange;
      };
      // service signal subscription
      struct
      {
         char *objPath;
         char *sigName;
         SVCIPC_tSignalCallback onSignal;
      };
   };
};

Subscription *svcipcNewOwnerChangedSubscription(struct Connection *conn, const char *busName,
      SVCIPC_tNameOwnerChangedCallback onNameChange, SVCIPC_tUserToken token);

Subscription *svcipcNewSignalSubscription(struct Connection *conn, const char *objPath,
      const char *sigName, SVCIPC_tSignalCallback onSignal, SVCIPC_tUserToken token);

void svcipcFreeSubscription(Subscription *sub);

#endif /* SUBSCRIPTION_H_ */

