/*
 * trace.c
 *
 *  Created on: Mar 31, 2012
 *      Author: AMckewan
 */

#include <process.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/slog.h>
#include <sys/slogcodes.h>

#include "trace.h"
#include "utils.h"

int svcipcTraceOutput = TRACE_OUTPUT_SLOG;
int svcipcTraceLevel = TRACE_LEVEL_WARN;

static uint64_t startTime;

int slogf( int opcode,
           int severity, 
           const char * fmt,
           ... )
{
    int retval=0;
    va_list ap;
    (void)opcode;
    (void)severity;

    va_start(ap, fmt); /* Initialize the va_list */

    retval = vprintf(fmt, ap); /* Call vprintf */

    va_end(ap); /* Cleanup the va_list */

    return retval;
}

void svcipcTraceInit(void)
{
   // check environment for trace settings
   char *value = getenv("SVCIPC_TRACE_LEVEL");
   if (value)
   {
      int level = atoi(value);
      if (level >= TRACE_LEVEL_NONE && level <= TRACE_LEVEL_INFO)
         svcipcTraceLevel = level;
      else
         slogf(_SLOG_SETCODE(_SLOGC_SVCIPC, 0), _SLOG_WARNING, "SVCIPC (pid %d) WARN: invalid SVCIPC_TRACE_LEVEL=%d", getpid(), level);
   }

   value = getenv("SVCIPC_TRACE_OUTPUT");
   if (value)
   {
      int output = atoi(value);
      if ((output & ~(TRACE_OUTPUT_SLOG|TRACE_OUTPUT_CONSOLE)) == 0)
         svcipcTraceOutput = output;
      else
         slogf(_SLOG_SETCODE(_SLOGC_SVCIPC, 0), _SLOG_WARNING, "SVCIPC (pid %d) WARN: invalid SVCIPC_TRACE_OUTPUT=%d", getpid(), output);
   }

   startTime = svcipcGetSystemTime();
}

static void trace(FILE *fp, int severity, const char *name, const char *fmt, va_list *va)
{
   if (svcipcTraceOutput & TRACE_OUTPUT_SLOG)
   {
      char msg[1000];
      vsnprintf(msg, sizeof msg, fmt, *va);
      slogf(_SLOG_SETCODE(_SLOGC_SVCIPC, 0), severity, "SVCIPC (pid %d) %s: %s", getpid(), name, msg);
   }
   if (svcipcTraceOutput & TRACE_OUTPUT_CONSOLE)
   {
      int now = (int) ((svcipcGetSystemTime() - startTime) % 100000ULL);
      fprintf(fp, "[%02d.%03d]%s: ", now / 1000, now % 1000, name);
      vfprintf(fp, fmt, *va);
      fputc('\n', fp);
      fflush(fp);
   }
}

void svcipcTraceInfo(const char *fmt, ...)
{
   va_list argList;
   va_start(argList, fmt);
   trace(stdout, _SLOG_INFO, "INFO", fmt, &argList);
   va_end(argList);
}

void svcipcTraceWarn(const char *fmt, ...)
{
   va_list argList;
   va_start(argList, fmt);
   trace(stderr, _SLOG_WARNING, "WARN", fmt, &argList);
   va_end(argList);
}

void svcipcTraceError(const char *fmt, ...)
{
   va_list argList;
   va_start(argList, fmt);
   trace(stderr, _SLOG_ERROR, "ERROR", fmt, &argList);
   va_end(argList);
}
