/*
 * context.c
 *
 *  Created on: Apr 8, 2012
 *      Author: AMckewan
 */

#include "context.h"
#include "connection.h"
#include <dbus/dbus.h>
#include <stdlib.h>
#include "trace.h"

struct Context
{
   Connection *conn;
   DBusMessage *msg;
};

static int allocated;

int svcipcGetAllocatedContexts(void)
{
   return allocated;
}

Context *svcipcNewContext(Connection *conn, struct DBusMessage *msg)
{
   Context *context = malloc(sizeof(Context));
   if (context)
   {
      context->conn = conn;
      context->msg = dbus_message_ref(msg);
      allocated++;
   }
   return context;
}

void svcipcFreeContext(Context *context)
{
   if (context)
   {
      if (context->msg)
         dbus_message_unref(context->msg);
      free(context);
      allocated--;
   }
}

SVCIPC_tError svcipcSendReply(Context *context, const char *result)
{
   SVCIPC_tError error = SVCIPC_ERROR_NO_MEMORY;
   Connection *conn = context->conn;

   DBusMessage *reply = dbus_message_new_method_return(context->msg);
   if (reply)
   {
      dbus_uint32_t serial;
      if (dbus_message_append_args(reply, DBUS_TYPE_STRING, &result, DBUS_TYPE_INVALID) && dbus_connection_send(
            svipcGetDBusConnection(conn), reply, &serial))
      {
         error = SVCIPC_ERROR_NONE;
      }
      dbus_message_unref(reply);
   }
   return error;
}

SVCIPC_tError svcipcSendError(Context *context, const char *errName, const char *errMsg)
{
   SVCIPC_tError error = SVCIPC_ERROR_NO_MEMORY;
   Connection *conn = context->conn;

   DBusMessage *reply = dbus_message_new_error(context->msg, errName, errMsg);
   if (reply)
   {
      dbus_uint32_t serial;
      if (dbus_connection_send(svipcGetDBusConnection(conn), reply, &serial))
      {
         error = SVCIPC_ERROR_NONE;
      }
      dbus_message_unref(reply);
   }
   return error;
}
