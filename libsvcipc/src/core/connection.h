/*
 * connection.h
 *
 *  Created on: Mar 31, 2012
 *      Author: AMckewan
 */

#ifndef CONNECTION_H_
#define CONNECTION_H_

#include "svcipc.h"


typedef struct Connection Connection;

Connection *svcipcGetConnection(SVCIPC_tConnType connType, SVCIPC_tBool openPrivate, DBusError *error);
int svcipcCloseConnection(Connection *conn);
int svcipcIsConnected(Connection *conn);

void svcipcDispatchPendingMessages(void);
DBusConnection *svipcGetDBusConnection(Connection *conn);

//void svcipcRegisterPending(svcipc_connection_t *conn, svcipc_command_t *cmd);
//void svcipcUnregisterPending(svcipc_connection_t *conn, svcipc_command_t *cmd);

struct Subscription;
void svcipcAddSubscription(struct Subscription *subscription);
void svcipcRemoveSubscription(struct Subscription *subscription);
int svcipcSubscriptionExists(struct Subscription *subscription);

struct Registration;
int svcipcAddRegistration(struct Registration *reg);
int svcipcRemoveRegistration(struct Registration *reg);
int svcipcRegistrationExists(struct Registration *reg);

#endif /* CONNECTION_H_ */
