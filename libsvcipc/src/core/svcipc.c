/*
 * svcipc.c
 *
 *  Created on: Apr 2, 2012
 *      Author: AMckewan
 */

#include "svcipc/svcipc.h"
#include "connection.h"
#include "context.h"
#include "dispatcher.h"
#include "trace.h"
#include "utils.h"
#include <stdlib.h>

////////////////////////////////////////////////////////////
// String constants

SVCIPC_tConstStr SVCIPC_ERR_NAME_OK = "com.harman.svcipc.error.Ok";
SVCIPC_tConstStr SVCIPC_ERR_NAME_NOT_SUPPORTED = "com.harman.svcipc.error.NotSupported";
SVCIPC_tConstStr SVCIPC_ERR_NAME_NO_MEMORY = "com.harman.svcipc.error.NoMemory";
SVCIPC_tConstStr SVCIPC_ERR_NAME_BAD_ARGS = "com.harman.svcipc.error.BadArgs";
SVCIPC_tConstStr SVCIPC_ERR_NAME_INTERNAL = "com.harman.svcipc.error.Internal";
SVCIPC_tConstStr SVCIPC_ERR_NAME_DBUS = "com.harman.svcipc.error.DBus";
SVCIPC_tConstStr SVCIPC_ERR_NAME_CMD_SUBMISSION = "com.harman.svcipc.error.CmdSubmission";
SVCIPC_tConstStr SVCIPC_ERR_NAME_NOT_CONNECTED = "com.harman.svcipc.error.NotConnected";
SVCIPC_tConstStr SVCIPC_ERR_NAME_CANCELLED = "com.harman.svcipc.error.Cancelled";
SVCIPC_tConstStr SVCIPC_ERR_NAME_CONN_SEND = "com.harman.svcipc.error.ConnectionSend";
SVCIPC_tConstStr SVCIPC_ERR_NAME_NOT_FOUND = "com.harman.svcipc.error.NotFound";
SVCIPC_tConstStr SVCIPC_ERR_NAME_DEADLOCK = "com.harman.svcipc.error.Deadlock";

const char SVCIPC_INTERFACE_NAME[] = "com.harman.ServiceIpc";
const char SVCIPC_INTERFACE_SIGNAL_NAME[] = "Emit";
const char SVCIPC_INTERFACE_METHOD_NAME[] = "Invoke";
const char SVCIPC_INTERFACE_ERROR_NAME[] = "com.harman.service.Error";
const char SVCIPC_INTERFACE_SIGNAL_SIGNATURE[] =
{ DBUS_TYPE_STRING, DBUS_TYPE_STRING, DBUS_TYPE_INVALID };
const char INTROSPECTION_INTERFACE_METHOD_NAME[] = "Introspect";

////////////////////////////////////////////////////////////
// Initialize

SVCIPC_tError SVCIPC_initialize(void)
{
   svcipcTraceInit();
   svcipcSetEnvironmentVariable();

   // Check if we should modify the dispatcher thread's priority
   int priority = 0;
   char *value = getenv("SVCIPC_DISPATCH_PRIORITY");
   if (value)
      priority = atoi(value);

   svcipcStartDispatcher(priority);

   return SVCIPC_ERROR_NONE;
}

void SVCIPC_shutdown(void)
{
   svcipcStopDispatcher();
   dbus_shutdown();

   int allocated = svcipcGetAllocatedContexts();
   if (allocated > 0)
      TRACE_WARN("SVCIPC_shutdown %d contexts not freed!!!", allocated);
}
