/*
 * watch.c
 *
 *  Created on: Mar 31, 2012
 *      Author: AMckewan
 */

#include <stdlib.h>
#include <dbus/dbus.h>
#include "trace.h"
#include "watch.h"

typedef struct Watch
{
   struct Watch *next;
   DBusWatch *watch;
   int fd;
   int enabled;
} Watch;

static Watch *watches;
static int numWatches;

//////////////////////////////////////////////////////////////////////

int svcipcNumWatches(void)
{
   return numWatches;
}

static void traceWatches(void)
{
/*
   TRACE_INFO("%d watches:", numWatches);
   Watch *w = watches;
   while (w)
   {
      TRACE_INFO("   w %p: watch=%p fd=%d enabled=%d next=%p", w, w->watch, w->fd, w->enabled, w->next);
      w = w->next;
   }
*/
}

//////////////////////////////////////////////////////////////////////
// Add/remove/toggle watches

static dbus_bool_t initWatch(Watch *w, DBusWatch *watch)
{
   dbus_watch_set_data(watch, w, 0);
   w->watch = watch;
   w->fd = dbus_watch_get_unix_fd(watch);
   w->enabled = dbus_watch_get_enabled(watch);
   numWatches++;
   TRACE_INFO("addWatch w=%p watch=%p fd=%d flags=%d enabled=%d", w, watch, w->fd, dbus_watch_get_flags(watch), w->enabled);
   traceWatches();
   return TRUE;
}

static dbus_bool_t addWatch(DBusWatch *watch, void *data)
{
   // see if we can find an unused watch
   Watch *w = watches;
   while (w)
   {
      if (w->watch == 0)
      {
         // re-use this w
         TRACE_INFO("  re-use existing watch w");
         return initWatch(w, watch);
      }
      w = w->next;
   }

   // allocate new watch
   w = malloc(sizeof(Watch));
   if (!w)
      return FALSE;

   // link into watch list
   w->next = watches;
   watches = w;

   return initWatch(w, watch);
}

static void removeWatch(DBusWatch *watch, void *data)
{
   Watch *w = dbus_watch_get_data(watch);
   if (w && w->watch == watch)
   {
      // just mark it unused by setting watch to 0
      dbus_watch_set_data(watch, 0, 0);
      w->watch = 0;
      w->enabled = FALSE;
      TRACE_INFO("removeWatch w=%p watch=%p fd=%d", w, watch, w->fd);
      numWatches--;
   }
   traceWatches();
}

static void toggleWatch(DBusWatch *watch, void *data)
{
   Watch *w = dbus_watch_get_data(watch);
   if (w && w->watch == watch)
   {
      TRACE_INFO("toggleWatch w=%p watch=%p", w, w->watch);
      w->enabled = dbus_watch_get_enabled(watch);
   }
}

int svcipcSetWatchFunctions(DBusConnection *dbusConn)
{
   return dbus_connection_set_watch_functions(dbusConn, addWatch, removeWatch, toggleWatch, 0, 0);
}

////////////////////////////////////////////////////////////
// Convert between dbus watch flags and poll events

static short getEvents(unsigned int flags)
{
   short events = 0;
   if (flags & DBUS_WATCH_READABLE)
      events |= POLLIN;
   if (flags & DBUS_WATCH_WRITABLE)
      events |= POLLOUT;
   return events;
}

static unsigned int getFlags(short events)
{
   unsigned int flags = 0;
   if (events & POLLIN)
      flags |= DBUS_WATCH_READABLE;
   if (events & POLLOUT)
      flags |= DBUS_WATCH_WRITABLE;
   if (events & POLLHUP)
      flags |= DBUS_WATCH_HANGUP;
   if (events & POLLERR)
      flags |= DBUS_WATCH_ERROR;
   return flags;
}

////////////////////////////////////////////////////////////
// Polling

nfds_t svcipcInitPollInfo(struct pollfd *pollfds)
{
   // build up a list of poll fds from the active watches
   nfds_t nfds = 0;
   Watch *w = watches;
   while (w)
   {
      //TRACE_INFO("checking watch %p", w);
      if (w->enabled)
      {
         pollfds[nfds].fd = w->fd;
         pollfds[nfds].events = getEvents(dbus_watch_get_flags(w->watch));
         nfds++;
      }
      w = w->next;
   }
   return nfds;
}

void svcipcHandleWatches(struct pollfd *pollfds, nfds_t nfds)
{
   // for each fd with events, find the watch and handle it
   TRACE_INFO("svcipcHandleWatches nfds=%d", nfds);
   nfds_t i;
   for (i = 0; i < nfds; i++)
   {
      int fd = pollfds[i].fd;
      unsigned int flags = getFlags(pollfds[i].revents);
      if (flags)
      {
         // handle watches that match this fd
         Watch *w = watches;
         while (w)
         {
            if (w->fd == fd && w->enabled)
            {
               //TRACE_INFO("handing watch %p", w->watch);
               dbus_watch_handle(w->watch, flags);
            }
            w = w->next;
         }
      }
   }
}
