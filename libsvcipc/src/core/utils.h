/*
 * utils.h
 *
 *  Created on: Mar 30, 2012
 *      Author: AMckewan
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <stdint.h>
#include <sys/slog.h>

char *svcipcBusNameToObjPath(const char *busName);
void svcipcSetEnvironmentVariable(void);
uint64_t svcipcGetSystemTime(void);
void svcipcSleep(int msec);
char *svcipcStrdup(const char *s);

#define _SLOGC_SVCIPC (_SLOGC_PRIVATE_START + 5000)
int svcipcSlog(int severity, const char *fmt, ...);

#endif /* UTILS_H_ */
