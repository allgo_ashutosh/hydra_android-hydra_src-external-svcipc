/*
 * subscription.c
 *
 *  Created on: Apr 3, 2012
 *      Author: AMckewan
 */

#include "subscription.h"
#include "trace.h"
#include "utils.h"

#include <process.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_RULE_SIZE 1000     // I'm lazy

void svcipcFreeSubscription(Subscription *sub)
{
   if (sub)
      sub->free(sub);
}

////////////////////////////////////////////////////////////
// Owner changed subscription

static char *ownerChangedRule(const char *busName)
{
   char buf[MAX_RULE_SIZE];
   if (busName && *busName)
   {
      snprintf(buf, MAX_RULE_SIZE, "type='signal',interface='%s',member='NameOwnerChanged',path='%s',arg0='%s'",
            DBUS_INTERFACE_DBUS, DBUS_PATH_DBUS, busName);
   }
   else
   {
      // no bus name, match on any bus name
      snprintf(buf, MAX_RULE_SIZE, "type='signal',interface='%s',member='NameOwnerChanged',path='%s'",
            DBUS_INTERFACE_DBUS, DBUS_PATH_DBUS);
   }
   return strdup(buf);
}

static int ownerChangedDispatchIfMatch(Subscription *sub, DBusMessage *msg, int timeout)
{
   int match = FALSE;
   const char *newName = 0;
   const char *oldOwner = 0;
   const char *newOwner = 0;

   if (dbus_message_is_signal(msg, DBUS_INTERFACE_DBUS, "NameOwnerChanged") && (dbus_message_has_path(msg,
         DBUS_PATH_DBUS)))
   {
      // Parse out the arguments for the signal
      if (dbus_message_get_args(msg, 0, DBUS_TYPE_STRING, &newName, DBUS_TYPE_STRING, &oldOwner, DBUS_TYPE_STRING,
            &newOwner, DBUS_TYPE_INVALID))
      {
         if (!sub->busName)
         {
            // don't care about the bus name
            match = TRUE;
         }
         else if (newName && !strcmp(newName, sub->busName))
         {
            match = TRUE;
         }
      }
   }

   // call users's callback if there is one (and why would there not be?)
   if (match && sub->onNameChange)
   {
      uint64_t now = svcipcGetSystemTime();
      sub->onNameChange(newName ? newName : "", oldOwner ? oldOwner : "", newOwner ? newOwner : "", sub->token);
      int elapsed = svcipcGetSystemTime() - now;
      if (elapsed > timeout)
      {
         svcipcSlog(_SLOG_WARNING, "Failed to process D-Bus OnNameChange within %d msec [PID=%u]", timeout, getpid());
      }
   }

   return match;
}

static void ownerChangedFree(Subscription *sub)
{
   free(sub->busName);
   free(sub->rule);
   free(sub);
}

Subscription *svcipcNewOwnerChangedSubscription(struct Connection *conn, const char *busName,
      SVCIPC_tNameOwnerChangedCallback onNameChange, SVCIPC_tUserToken token)
{
   Subscription *sub = malloc(sizeof(Subscription));
   if (sub)
   {
      sub->dispatchIfMatch = ownerChangedDispatchIfMatch;
      sub->free = ownerChangedFree;

      sub->rule = ownerChangedRule(busName);
      sub->conn = conn;
      sub->token = token;

      sub->busName = busName ? strdup(busName) : 0;
      sub->onNameChange = onNameChange;
   }
   return sub;
}

////////////////////////////////////////////////////////////
// signal subscription

static char *signalRule(const char *objPath, const char *sigName)
{
   char buf[MAX_RULE_SIZE];
   snprintf(buf, MAX_RULE_SIZE, "type='signal',interface='%s',member='%s',path='%s',arg0='%s'", SVCIPC_INTERFACE_NAME,
         SVCIPC_INTERFACE_SIGNAL_NAME, objPath, sigName);
   return strdup(buf);
}

static int signalDispatchIfMatch(Subscription *sub, DBusMessage *msg, int timeout)
{
   const char *name = 0;
   const char *data = 0;
   int match = FALSE;

   if (dbus_message_is_signal(msg, SVCIPC_INTERFACE_NAME, SVCIPC_INTERFACE_SIGNAL_NAME) && (dbus_message_has_path(msg,
         sub->objPath)))
   {
      if (dbus_message_get_args(msg, 0, DBUS_TYPE_STRING, &name, DBUS_TYPE_STRING, &data, DBUS_TYPE_INVALID))
      {
         if (name && !strcmp(name, sub->sigName))
         {
            match = TRUE;

            uint64_t now = svcipcGetSystemTime();
            sub->onSignal(name, data ? data : "", sub->token);
            int elapsed = svcipcGetSystemTime() - now;
            if (elapsed > timeout)
            {
               svcipcSlog(_SLOG_WARNING, "Failed to process D-Bus signal (%s) within %d msec [PID=%u]", name, timeout,
                     getpid());
            }
         }
      }
   }
   return match;
}

static void signalFree(Subscription *sub)
{
   free(sub->objPath);
   free(sub->sigName);
   free(sub->rule);
   free(sub);
}

Subscription *svcipcNewSignalSubscription(struct Connection *conn, const char *objPath,
      const char *sigName, SVCIPC_tSignalCallback onSignal, SVCIPC_tUserToken token)
{
   Subscription *sub = malloc(sizeof(Subscription));
   if (sub)
   {
      sub->dispatchIfMatch = signalDispatchIfMatch;
      sub->free = signalFree;

      sub->rule = signalRule(objPath, sigName);
      sub->conn = conn;
      sub->token = token;

      sub->objPath = strdup(objPath);
      sub->sigName = strdup(sigName);
      sub->onSignal = onSignal;
   }
   return sub;
}
