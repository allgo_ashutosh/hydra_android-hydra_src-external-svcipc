/*
 * watch.h
 *
 *  Created on: Mar 31, 2012
 *      Author: AMckewan
 */

#ifndef WATCH_H_
#define WATCH_H_

#include <sys/poll.h>

int svcipcSetWatchFunctions(DBusConnection *dbusConn);
int svcipcNumWatches(void);
nfds_t svcipcInitPollInfo(struct pollfd *pollfds);
void svcipcHandleWatches(struct pollfd *pollfds, nfds_t nfds);

#endif /* WATCH_H_ */
