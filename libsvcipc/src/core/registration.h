/*
 * registration.h
 *
 *  Created on: Apr 7, 2012
 *      Author: AMckewan
 */

#ifndef REGISTRATION_H_
#define REGISTRATION_H_

#include "svcipc/svcipc.h"

struct DBusMessage;
struct Connection;

typedef struct Registration Registration;

struct Registration
{
   Registration *next;

   struct Connection *conn;
   char *busName;
   char *objPath;
   SVCIPC_tUInt32 flag;
   SVCIPC_tRequestCallback onRequest;
   SVCIPC_tUserToken token;
};

Registration *svcipcNewRegistration(SVCIPC_tConnection conn, char *busName, char *objPath,
      SVCIPC_tUInt32 flag, SVCIPC_tRequestCallback onRequest, SVCIPC_tUserToken token);

void svcipcFreeRegistration(Registration *reg);

void svcipcDispatchRequest(Registration *reg, struct DBusMessage *msg, const char *method, const char *args,
      int timeout);

#endif /* REGISTRATION_H_ */
