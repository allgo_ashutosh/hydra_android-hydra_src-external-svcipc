/*
 * dispatcher.h
 *
 *  Created on: Mar 30, 2012
 *      Author: AMckewan
 */

#ifndef DISPATCHER_H_
#define DISPATCHER_H_

#include "svcipc/svcipc.h"

struct Command;

int svcipcStartDispatcher(int priority);
void svcipcStopDispatcher(void);
int svcipcDispatcherIsRunning(void);

SVCIPC_tError svcipcSubmitCommand(struct Command *cmd);
SVCIPC_tError svcipcSubmitCommandAndWait(struct Command *cmd);
void svcipcPostResult(struct Command *cmd, SVCIPC_tError error);

#endif /* DISPATCHER_H_ */
