/*
 * dispatcher.c
 *
 *  Created on: Mar 30, 2012
 *      Author: AMckewan
 */

#include "dispatcher.h"

#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include "command.h"
#include "connection.h"
#include "timeout.h"
#include "trace.h"
#include "utils.h"
#include "watch.h"

#define USE_PIPE 1

static const int MAX_POLL_TIMEOUT = 3000; // Default amount of time to block in a call to poll()

static pthread_t tid = -1;
static int running = FALSE;
static int quit = FALSE;
#if USE_PIPE
static int pipefds[2] =
{ -1, -1 };
#endif

int svcipcDispatcherIsRunning(void)
{
   return running;
}

//////////////////////////////////////////////////////////////////////
// Execute dispatcher

static int processCommands(void)
{
   int n = 0;
   Command *cmd;
   while ((cmd = svcipcCommandQueueRemove()) != 0)
   {
#if USE_PIPE
      char dummy[1];
      read(pipefds[0], dummy, 1);
#endif

      cmd->execute(cmd, cmd->conn);
      n++;
   }
   if (n > 0)
      TRACE_INFO("processed %d command(s)", n);
   return n;
}

static void dispatch(void)
{
   uint64_t pollStartTime, pollEndTime;
   int timeout, elapsed, selected;

   // allocate and initialize pollfds
   nfds_t nfds = svcipcNumWatches();
   struct pollfd *pollfds = malloc((nfds + 1) * sizeof(struct pollfd));
   if (!pollfds)
   {
      TRACE_ERROR("dispatcher out of memory\n");
      sleep(1);
      return;
   }
   nfds = svcipcInitPollInfo(pollfds);

#if USE_PIPE
   // add pipe fd at end of poll list
   pollfds[nfds].fd = pipefds[0];
   pollfds[nfds].events = POLLIN;
   nfds++;
#endif

   // calculate poll timeout
   pollStartTime = svcipcGetSystemTime();
   timeout = svcipcPollTimeout(pollStartTime, MAX_POLL_TIMEOUT);

   // poll for active file descriptors
   TRACE_INFO("poll nfds=%d, timeout=%d", nfds, timeout);
   selected = poll(pollfds, nfds, timeout);

   pollEndTime = svcipcGetSystemTime();
   elapsed = pollEndTime - pollStartTime;
   TRACE_INFO("poll selected=%d, errno=%d, elapsed=%d", selected, selected < 0 ? errno : 0, elapsed);

   // If there was an error polling the file descriptors and if we haven't waited at least
   // a minimum amount of time, then delay so we don't (potentially) consume all available
   // CPU cycles. Ignore EINTR because that likely means we have commands to process.
   if (selected < 0 && errno != EINTR && elapsed < timeout && !quit)
   {
      delay(timeout - elapsed);
      pollEndTime = svcipcGetSystemTime();
      elapsed = pollEndTime - pollStartTime;
   }

   if (elapsed >= timeout)
      svcipcHandleTimeouts(pollEndTime);

   if (selected > 0)
   {
#if USE_PIPE
      // check pipe poll
      if ((pollfds[nfds - 1].revents & POLLIN))
      {
         processCommands();
         selected--;
      }
      if (selected > 0)
#endif
         svcipcHandleWatches(pollfds, nfds);
   }

   free(pollfds);
}

int svcipcDispatcherExecute()
{
   svcipcDispatchPendingMessages();
#if USE_PIPE
   dispatch();
#else
   processCommands();
   dispatch();
   processCommands();
#endif
   return !quit;
}

////////////////////////////////////////////////////////////////////////////
// Dispatcher thread

#if !USE_PIPE
static void handler(int signo)
{
   // dummy signal handler
}
#endif

static void svcipcDispatcherInit(void)
{
#if USE_PIPE
   pipe(pipefds);
   fcntl(pipefds[1], F_SETFD, O_NONBLOCK);
#else
   struct sigaction act;
   sigset_t set;

   sigemptyset(&set);
   sigaddset(&set, SIGUSR1);

   act.sa_handler = handler;
   act.sa_flags = 0;
   act.sa_mask = set;
   sigaction(SIGUSR1, &act, NULL);
#endif
}

static void signalThread(void)
{
#if USE_PIPE
      write(pipefds[1], pipefds, 1);
#else
      pthread_kill(tid, SIGUSR1);
#endif
}

static void *dispatcherThread(void *arg)
{
   //struct sched_param param;
   //pthread_getschedparam(pthread_self(), NULL, &param);
   //TRACE_INFO("dispatcher thread started, priority = %d", param.sched_curpriority);

   svcipcDispatcherInit();
   while (svcipcDispatcherExecute())
   {
      // nothing
   }

   TRACE_INFO("dispatcher thread ended");
   return 0;
}

int svcipcStartDispatcher(int priority)
{
   if (!running)
   {
      pthread_attr_t attr;
      pthread_attr_init(&attr);
      if (priority > 0)
      {
         struct sched_param param;
         param.sched_priority = priority;
         pthread_attr_setschedparam(&attr, &param);
#if 0
         pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
#endif
      }

      quit = FALSE;
      int ret = pthread_create(&tid, &attr, dispatcherThread, NULL);
      if (ret == EOK)
      {
         running = TRUE;
      }
      else
      {
         fprintf(stderr, "pthread_create: %s\n", strerror(ret));
      }
   }
   return running;
}

void svcipcStopDispatcher(void)
{
   if (running)
   {
      TRACE_INFO("stopping dispatcher thread");
      quit = TRUE;
      signalThread();
      pthread_join(tid, NULL);
      running = FALSE;
   }
}

////////////////////////////////////////////////////////////////////////////
// Submit commands to the dispatcher

SVCIPC_tError svcipcSubmitCommand(Command *cmd)
{
   //TRACE_INFO("svcipcSubmitCommand");
   if (running)
   {
      svcipcCommandQueueAdd(cmd);
      signalThread();
      return SVCIPC_ERROR_NONE;
   }
   return SVCIPC_ERROR_BAD_ARGS;
}

SVCIPC_tError svcipcSubmitCommandAndWait(Command *cmd)
{
   TRACE_INFO("svcipcSubmitCommandAndWait");
   SVCIPC_tError error = SVCIPC_ERROR_BAD_ARGS;
   if (running)
   {
      sem_t sem;

      sem_init(&sem, 0, 0);
      cmd->sem = &sem;
      cmd->error = &error;

      svcipcCommandQueueAdd(cmd);
      signalThread();

      sem_wait(&sem);
      sem_destroy(&sem);
   }
   return error;
}

void svcipcPostResult(Command *cmd, SVCIPC_tError error)
{
   if (cmd->error)
      *cmd->error = error;
   sem_post(cmd->sem);
}
