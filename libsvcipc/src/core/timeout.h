/*
 * timeout.h
 *
 *  Created on: Mar 31, 2012
 *      Author: AMckewan
 */

#ifndef TIMEOUT_H_
#define TIMEOUT_H_

#include <stdint.h>

int svcipcSetTimeoutFunctions(DBusConnection *dbusConn);
int svcipcPollTimeout(uint64_t now, int maxTimeout);
void svcipcHandleTimeouts(uint64_t now);


#endif /* TIMEOUT_H_ */
