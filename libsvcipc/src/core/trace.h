/*
 * trace.h
 *
 *  Created on: Mar 31, 2012
 *      Author: AMckewan
 */

#ifndef TRACE_H_
#define TRACE_H_

// svcipcTraceOutput determines where trace output goes: slog, console or both
extern int svcipcTraceOutput;

#define TRACE_OUTPUT_SLOG     1
#define TRACE_OUTPUT_CONSOLE  2

// svcipcTraceLevel contains the tracing level
extern int svcipcTraceLevel;

#define TRACE_LEVEL_NONE      0
#define TRACE_LEVEL_ERROR     1
#define TRACE_LEVEL_WARN      2
#define TRACE_LEVEL_INFO      3

void svcipcTraceInit(void);
void svcipcTraceInfo(const char *fmt, ...);
void svcipcTraceWarn(const char *fmt, ...);
void svcipcTraceError(const char *fmt, ...);

#define TRACE_TEST(...)    do { svcipcTraceInfo(__VA_ARGS__); } while (0)
#define TRACE_INFO(...)    do { if (svcipcTraceLevel >= TRACE_LEVEL_INFO)  svcipcTraceInfo(__VA_ARGS__); } while (0)
#define TRACE_WARN(...)    do { if (svcipcTraceLevel >= TRACE_LEVEL_WARN)  svcipcTraceWarn(__VA_ARGS__); } while (0)
#define TRACE_ERROR(...)   do { if (svcipcTraceLevel >= TRACE_LEVEL_ERROR) svcipcTraceError(__VA_ARGS__); } while (0)


#endif /* TRACE_H_ */
