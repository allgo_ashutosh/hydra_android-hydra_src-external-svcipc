/*
 * utils.c
 *
 *  Created on: Mar 30, 2012
 *      Author: AMckewan
 */

#include <ctype.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/slogcodes.h>
#include <time.h>
#include <unistd.h>
#include "utils.h"

// Convert a bus name to an object path.
// Prepend with '/', replace all '.' with '/', replace other non-alphanumeric characters with '_'
// This always returns a malloc'd string, must free later.
char *svcipcBusNameToObjPath(const char *busName)
{
   // objPath is 1 character longer than busName because we add '/' at the beginning
   int i = 0;
   int len = busName ? strlen(busName) : 0;
   char *objPath = malloc(len + 2);

   objPath[0] = '/';
   while (i < len)
   {
      int c = busName[i++];
      if (isalnum(c))
      {
         objPath[i] = c;
      }
      else if (c == '.')
      {
         objPath[i] = '/';
      }
      else
      {
         objPath[i] = '_';
      }
   }
   objPath[i + 1] = 0;
   return objPath;
}

void svcipcSetEnvironmentVariable(void)
{
   // A ridiculously large bus address;
   static const int MAX_DBUS_BUS_ADDRESS_SIZE = 1024;
   char busAddress[MAX_DBUS_BUS_ADDRESS_SIZE + 1];
   char scanString[256];

   // Check if DBUS environment variable has already been set
   if (!getenv("DBUS_SESSION_BUS_ADDRESS"))
   {
      char *environmentShellName = getenv("DBUS_SCRIPT_FILE_NAME");
      if (!environmentShellName)
      {
         environmentShellName = "/tmp/envars.sh";
      }
      FILE *fd = fopen(environmentShellName, "r");
      if (fd)
      {
         snprintf(scanString, sizeof(scanString), "DBUS_SESSION_BUS_ADDRESS='%%%ds';", MAX_DBUS_BUS_ADDRESS_SIZE);
         fscanf(fd, scanString, busAddress);

         if ((strlen(busAddress) < MAX_DBUS_BUS_ADDRESS_SIZE) && (strlen(busAddress) > 2))
         {
            if (strcmp(&busAddress[strlen(busAddress) - 2], "';") == 0)
            {
               busAddress[strlen(busAddress) - 2] = '\0';
            }

            setenv("DBUS_SESSION_BUS_ADDRESS", busAddress, 0);
         }

         fclose(fd);
      }
   }
}

uint64_t svcipcGetSystemTime(void)
{
   struct timespec now;
   uint64_t msecTime = 0;

   if (-1 != clock_gettime(CLOCK_MONOTONIC, &now))
   {
      msecTime = (now.tv_sec * 1000U) + (now.tv_nsec / 1000000U);
   }

   return msecTime;
}

void svcipcSleep(int msec)
{
   usleep(msec * 1000);
}

char *svcipcStrdup(const char *s)
{
   return s ? strdup(s) : 0;
}

int svcipcSlog(int severity, const char *fmt, ...)
{
#if 0
   va_list arg;
   va_start(arg, fmt);
   int32_t result = vslogf(_SLOG_SETCODE(_SLOGC_SVCIPC, 0), severity, fmt, arg);
   va_end(arg);
   return result;
#else
   return 0; 
#endif
}
