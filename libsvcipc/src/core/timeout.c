/*
 * timeout.c
 *
 *  Created on: Mar 31, 2012
 *      Author: AMckewan
 */

#include <stdlib.h>
#include <dbus/dbus.h>
#include "timeout.h"
#include "trace.h"
#include "utils.h"

/*
 * Like watches, we don't actually delete removed timeouts because we don't want
 * to disrupt the iteration while handling timeouts. Since handling may add or remove
 * a timeout, we just mark deleted timeouts as available and don't move any memory.
 */

typedef struct Timeout
{
   struct Timeout *next;        // link to next timeout
   DBusTimeout *timeout;            // dbus timeout or NULL if this t is available
   uint64_t expiry;                 // absolute time in milliseconds when this timeout expires
   int enabled;                     // true if this timeout is enabled
} Timeout;

static Timeout *timeouts;    // list of timeouts


//////////////////////////////////////////////////////////////////////
// Add/remove/toggle timeouts

static void setExpiry(Timeout *t, DBusTimeout *timeout)
{
   t->enabled = dbus_timeout_get_enabled(timeout);
   if (t->enabled)
   {
      int interval = dbus_timeout_get_interval(timeout);
      t->expiry = svcipcGetSystemTime() + (uint64_t)interval;
   }
}

static dbus_bool_t initNode(Timeout *t, DBusTimeout *timeout)
{
   dbus_timeout_set_data(timeout, t, 0);
   t->timeout = timeout;
   setExpiry(t, timeout);
   TRACE_INFO("add timeout enabled=%d, interval=%d", t->enabled, dbus_timeout_get_interval(timeout));
   return TRUE;
}

static dbus_bool_t addTimeout(DBusTimeout *timeout, void *data)
{
   // see if we can find an unused watch
   Timeout *t = timeouts;
   while (t)
   {
      if (t->timeout == NULL)
      {
         // re-use this t
         return initNode(t, timeout);
      }
      t = t->next;
   }
   t = malloc(sizeof(Timeout));
   dbus_timeout_set_data(timeout, t, 0);
   t->next = timeouts;
   timeouts = t;
   return initNode(t, timeout);
}

static void removeTimeout(DBusTimeout *timeout, void *data)
{
   // just mark it unused
   Timeout *t = dbus_timeout_get_data(timeout);
   if (t && t->timeout == timeout)
   {
      dbus_timeout_set_data(timeout, 0, 0);
      t->timeout = NULL;
      t->enabled = FALSE;
      TRACE_INFO("remove timeout");
   }
}

static void toggleTimeout(DBusTimeout *timeout, void *data)
{
   Timeout *t = dbus_timeout_get_data(timeout);
   if (t && t->timeout == timeout)
   {
      setExpiry(t, timeout);
      TRACE_INFO("toggle timeout enabled=%d, interval=%d", t->enabled, dbus_timeout_get_interval(timeout));
   }
}

int svcipcSetTimeoutFunctions(DBusConnection *dbusConn)
{
   return dbus_connection_set_timeout_functions(dbusConn, addTimeout, removeTimeout, toggleTimeout, 0, 0);
}

//////////////////////////////////////////////////////////////////////
// Calculate maximum time to wait in poll

int svcipcPollTimeout(uint64_t now, int maxTimeout)
{
   uint64_t limit = now + (uint64_t) maxTimeout;
   Timeout *t = timeouts;
   while (t)
   {
      if (t->enabled)
      {
         // if timeout is already expired, don't wait at all
         if (t->expiry <= now)
         {
            return 0;
         }

         // if the timeout expires before the limit, reduce the limit
         if (t->expiry < limit)
         {
            limit = t->expiry;
         }
      }
      t = t->next;
   }
   return (int)(limit - now);
}

//////////////////////////////////////////////////////////////////////
// Handle any timeouts that have expired

void svcipcHandleTimeouts(uint64_t now)
{
   Timeout *t = timeouts;
   while (t)
   {
      if (t->enabled && now >= t->expiry)
      {
         dbus_timeout_handle(t->timeout);
      }
      t = t->next;
   }
}
