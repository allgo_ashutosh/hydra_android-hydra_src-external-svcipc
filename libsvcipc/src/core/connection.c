/*
 * connection.c
 *
 *  Created on: Mar 31, 2012
 *      Author: AMckewan
 */

#include "connection.h"
#include "subscription.h"
#include "registration.h"
#include "timeout.h"
#include "trace.h"
#include "utils.h"
#include "watch.h"
#include <stdlib.h>
#include <string.h>

struct Connection
{
   DBusConnection *dbusConn;
   Subscription *subscriptions;
   Registration *registrations;
   int maxDispatchProcTime;
   int pending;
   int refcnt; // # references to the connection (see get/close)
};

// The one-and-only connection
static Connection theConnection;

//////////////////////////////////////////////////////////////////////

int svcipcIsConnected(Connection *conn)
{
   return (conn == &theConnection) && (conn->refcnt > 0) && dbus_connection_get_is_connected(conn->dbusConn);
}

DBusConnection *svipcGetDBusConnection(Connection *conn)
{
   return conn->dbusConn;
}

//////////////////////////////////////////////////////////////////////
// Handle dbus callbacks

static void onDispatchStatusUpdate(DBusConnection *dbusConn, DBusDispatchStatus status, void *data)
{
   Connection *conn = (Connection *) data;
   switch (status)
   {
   case DBUS_DISPATCH_DATA_REMAINS:
      TRACE_INFO("onDispatchStatusUpdate: needs dispatching on dbusConn=%p", dbusConn);
      conn->pending = TRUE;
      break;

   case DBUS_DISPATCH_COMPLETE:
      TRACE_INFO("onDispatchStatusUpdate: dispatch complete for dbusConn=%p", dbusConn);
      break;

   case DBUS_DISPATCH_NEED_MEMORY:
      TRACE_WARN("onDispatchStatusUpdate: dispatch needs memory for dbusConn=%p", dbusConn);
      break;

   default:
      TRACE_WARN("onDispatchStatusUpdate: Unknown status = %d", status);
      break;
   }
}

static DBusHandlerResult messageFilter(DBusConnection *dbusConn, DBusMessage *msg, void *data)
{
   Connection *conn = (Connection *) data;
   DBusHandlerResult result = DBUS_HANDLER_RESULT_NOT_YET_HANDLED;

   if (dbus_message_is_signal(msg, DBUS_INTERFACE_LOCAL, "Disconnected") && dbus_message_has_path(msg, DBUS_PATH_LOCAL))
   {
      TRACE_INFO("messageFilter: %p disconnected by local bus", dbusConn);
      /*
       if ( mPrivate )
       {
       dbus_connection_close(dbusConn);
       }
       */
      result = DBUS_HANDLER_RESULT_HANDLED;
   }
   else if (dbus_message_get_type(msg) == DBUS_MESSAGE_TYPE_SIGNAL)
   {
      // Dispatch the signal to all subscribers. There can be more than
      // one subscriber for each signal.
      Subscription *sub = conn->subscriptions;
      while (sub)
      {
         if (sub->dispatchIfMatch(sub, msg, conn->maxDispatchProcTime))
         {
            result = DBUS_HANDLER_RESULT_HANDLED;
         }
         sub = sub->next;
      }
   }
   // See if this is an incoming request (e.g. we're hosting services)
   else if (dbus_message_is_method_call(msg, SVCIPC_INTERFACE_NAME, SVCIPC_INTERFACE_METHOD_NAME))
   {
      char *method, *args;
      // Fish out the (actual) method name and JSON encoded args
      if (dbus_message_get_args(msg, 0, DBUS_TYPE_STRING, &method, DBUS_TYPE_STRING, &args, DBUS_TYPE_INVALID))
      {
         // Find the service that should receive this message
         Registration *reg = conn->registrations;
         while (reg)
         {
            if (dbus_message_has_path(msg, reg->objPath))
            {
               svcipcDispatchRequest(reg, msg, method, args, conn->maxDispatchProcTime);
               result = DBUS_HANDLER_RESULT_HANDLED;
               break;
            }
            reg = reg->next;
         }
      }
      else
      {
         TRACE_ERROR("messageFilter: failed to decode method arguments");
      }
   }
   else if (dbus_message_is_method_call(msg, DBUS_INTERFACE_INTROSPECTABLE, INTROSPECTION_INTERFACE_METHOD_NAME))
   {
      //result = introspect(msg);
   }

   return result;
}

//////////////////////////////////////////////////////////////////////
// create and delete connection object

static Connection *initConnection(Connection *conn, DBusConnection *dbusConn)
{
   conn->dbusConn = dbusConn;
   conn->subscriptions = 0;
   conn->registrations = 0;
   conn->maxDispatchProcTime = INT32_MAX;
   conn->pending = TRUE;
   conn->refcnt = 0;

   // get max dispatch time from environment
   char *value = getenv("SVCIPC_MAX_DISPATCH_PROC_TIME_MSEC");
   if (value)
   {
      conn->maxDispatchProcTime = atoi(value);
      TRACE_INFO("setting maxDispatchProcTime to %d", conn->maxDispatchProcTime);
   }

   // We don't want the the program to exit when the connection is disconnected.
   dbus_connection_set_exit_on_disconnect(dbusConn, FALSE);
   dbus_connection_set_dispatch_status_function(dbusConn, onDispatchStatusUpdate, conn, 0);

   svcipcSetWatchFunctions(dbusConn);
   svcipcSetTimeoutFunctions(dbusConn);

   if (!dbus_connection_add_filter(dbusConn, messageFilter, conn, 0))
   {
      TRACE_ERROR("dbus_connection_add_filter error");
      return 0;
   }

   TRACE_INFO("init connection %p dbusConn=%p", conn, dbusConn);
   conn->refcnt = 1;
   return conn;
}

static void freeConnection(Connection *conn)
{
   // free subscriptions
   Subscription *sub = conn->subscriptions;
   while (sub)
   {
      Subscription *next = sub->next;
      svcipcFreeSubscription(sub);
      sub = next;
   }

   // free registrations
   Registration *reg = conn->registrations;
   while (reg)
   {
      Registration *next = reg->next;
      svcipcFreeRegistration(reg);
      reg = next;
   }

   // clear connection object
   memset(conn, 0, sizeof(*conn));
}

static void closeConnection(Connection *conn)
{
   TRACE_INFO("closing connection %p dbusConn=%p", conn, conn->dbusConn);
   DBusConnection *dbusConn = conn->dbusConn;
   if (dbus_connection_get_is_connected(dbusConn))
   {
      DBusError error;
      dbus_error_init(&error);

      // Remove any outstanding subscriptions
      Subscription *sub = conn->subscriptions;
      while (sub)
      {
         dbus_bus_remove_match(dbusConn, sub->rule, &error);
         sub = sub->next;
      }

      // Release any well-known bus names we might own
      Registration *reg = conn->registrations;
      while (reg)
      {
         dbus_bus_release_name(dbusConn, reg->busName, &error);
         reg = reg->next;
      }

      // Flush the out-going message queue
      dbus_connection_flush(dbusConn);
      dbus_error_free(&error);
   }

   dbus_connection_remove_filter(dbusConn, messageFilter, conn);

   // We also need to explicitly decrement the reference count on the
   // associated underlying D-Bus connection object. This *could* be
   // the last reference to this connection object so we don't want to
   // reference it again from this point forward.
   dbus_connection_unref(dbusConn);

   freeConnection(conn);
}

//////////////////////////////////////////////////////////////////////
// open/close connection

Connection *svcipcGetConnection(SVCIPC_tConnType connType, SVCIPC_tBool openPrivate, DBusError *error)
{
   Connection *conn = &theConnection;
   if (conn->refcnt == 0)
   {
      // ignore connType and openPrivate (svcipc only works one way)
      DBusConnection *dbusConn = dbus_bus_get(DBUS_BUS_SESSION, error);
      if (!dbusConn)
      {
         TRACE_ERROR("dbus_bus_get error : %s : %s", error->name, error->message);
         return 0;
      }

      return initConnection(conn, dbusConn);
   }
   else
   {
      conn->refcnt++;
      return conn;
   }
}

int svcipcCloseConnection(Connection *conn)
{
   if (conn == &theConnection && conn->refcnt > 0)
   {
      // decrement reference count, if zero then close the connection
      if (--conn->refcnt == 0)
      {
         closeConnection(conn);
      }
      return TRUE;
   }
   else
   {
      TRACE_ERROR("attempt to close invalid connection %p", conn);
      return FALSE;
   }
}

//////////////////////////////////////////////////////////////////////
// dispatch messages

static void dispatchMessages(Connection *conn)
{
   DBusDispatchStatus status = DBUS_DISPATCH_COMPLETE;

   // Only dispatch messages while we're connected
   if (dbus_connection_get_is_connected(conn->dbusConn))
   {
      // Dispatch messages while data remains to be processed
      do
      {
         status = dbus_connection_dispatch(conn->dbusConn);
      } while (status == DBUS_DISPATCH_DATA_REMAINS);

      // If the dispatch indicates there was an error
      //(DBUS_DISPATCH_NEED_MEMORY) then we won't say the dispatch was
      // complete. Perhaps by the next go-around memory will have been freed
      // and the messages can be delivered.
   }

   if (status == DBUS_DISPATCH_COMPLETE)
      conn->pending = FALSE;
}

void svcipcDispatchPendingMessages(void)
{
   Connection *conn = &theConnection;
   if (conn->pending)
   {
      dispatchMessages(conn);
   }
}

//////////////////////////////////////////////////////////////////////
// Manage subscriptions

void svcipcAddSubscription(struct Subscription *subscription)
{
   Connection *conn = &theConnection;
   if (conn == subscription->conn)
   {
      // link into our subscription list
      subscription->next = conn->subscriptions;
      conn->subscriptions = subscription;
   }
   else
   {
      TRACE_ERROR("svcipcAddSubscription invalid connection %p", subscription->conn);
   }
}

void svcipcRemoveSubscription(struct Subscription *subscription)
{
   // this is ok if subscription is null (there's a check in svcipcFreeSubscription)
   Connection *conn = &theConnection;
   Subscription *sub = conn->subscriptions;
   if (sub)
   {
      if (sub == subscription)
      {
         conn->subscriptions = sub->next;
      }
      else
      {
         // search for subscription to remove
         Subscription *next = sub->next;
         while (next)
         {
            if (next == subscription)
            {
               sub->next = next->next;
               break;
            }
            sub = next;
            next = sub->next;
         }
      }
   }
   svcipcFreeSubscription(subscription);
}

int svcipcSubscriptionExists(struct Subscription *subscription)
{
   Connection *conn = &theConnection;
   Subscription *sub = conn->subscriptions;
   while (sub)
   {
      if (sub == subscription)
         return TRUE;
      sub = sub->next;
   }
   return FALSE;
}

//////////////////////////////////////////////////////////////////////
// Service registrations

int svcipcAddRegistration(struct Registration *reg)
{
   Connection *conn = &theConnection;
   if (conn && conn == reg->conn)
   {
      // link into registration list
      reg->next = conn->registrations;
      conn->registrations = reg;
      return TRUE;
   }
   else
   {
      TRACE_ERROR("svcipcRegisterService invalid connection %p", reg->conn);
      return FALSE;
   }
}

int svcipcRemoveRegistration(struct Registration *reg)
{
   Connection *conn = &theConnection;
   if (conn && conn == reg->conn)
   {
      // remove from registration list and free registration object
      Registration **it = &conn->registrations;
      while (*it)
      {
         if (*it == reg)
         {
            *it = (*it)->next;
            break;
         }
         it = &((*it)->next);
      }

      svcipcFreeRegistration(reg);
      return TRUE;
   }
   else
   {
      TRACE_ERROR ("svcipcUnregisterService unknown registration %p", reg);
      // don't free registration in case it is invalid
      return FALSE;
   }

}

int svcipcRegistrationExists(struct Registration *reg)
{
   Connection *conn = &theConnection;
   if (conn)
   {
      Registration *it = conn->registrations;
      while (it)
      {
         if (it == reg)
            return TRUE;
         it = it->next;
      }
   }
   return FALSE;
}

