/*
 * command.c
 *
 *  Created on: Mar 30, 2012
 *      Author: AMckewan
 */

#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include "command.h"

void *svcipcCommand(int size, void (*execute)(), Connection *conn)
{
   Command *cmd = malloc(size);
   if (cmd)
   {
      memset(cmd, 0, size);
      cmd->execute = execute;
      cmd->sem = NULL;
      cmd->error = NULL;
      cmd->conn = conn;
   }
   return cmd;
}


/*
 * An empty queue is indicated with firstCommand = 0
 * lastCommand is only valid for a non-empty queue
 */
static Command *firstCommand, *lastCommand;
static pthread_mutex_t queueMutex = PTHREAD_MUTEX_INITIALIZER;

int svcipcCommandQueueIsEmpty(void)
{
   return (firstCommand == 0);
}

void svcipcCommandQueueAdd(Command *cmd)
{
   cmd->next = 0;
   pthread_mutex_lock(&queueMutex);
   if (firstCommand)
   {
      lastCommand->next = cmd;
   }
   else
   {
      firstCommand = cmd;
   }
   lastCommand = cmd;
   pthread_mutex_unlock(&queueMutex);
}

Command *svcipcCommandQueueRemove(void)
{
   Command *cmd = 0;
   pthread_mutex_lock(&queueMutex);
   if (firstCommand)
   {
      cmd = firstCommand;
      firstCommand = cmd->next;
   }
   pthread_mutex_unlock(&queueMutex);
   return cmd;
}
