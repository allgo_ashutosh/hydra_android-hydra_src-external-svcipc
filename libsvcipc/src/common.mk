# This is an automatically generated record.
# The area between QNX Internal Start and QNX Internal End is controlled by
# the QNX IDE properties.

ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)

define PINFO
PINFO DESCRIPTION=Service IPC library.
PINFO VERSION=2.0.0
endef

#===== USEFILE - the file containing the usage message for the application. 
USEFILE=

#===== EXTRA_SRCVPATH - a space-separated list of directories to search for source files.
EXTRA_SRCVPATH+=$(PROJECT_ROOT)/core  \
	$(PROJECT_ROOT)/commands

#===== NAME - name of the project (default - name of project directory).
NAME=svcipc

#===== LIBS - a space-separated list of library items to be included in the link.
LIBS+=dbus-1 socket

#===== SO_VERSION - version number for SONAME (see -h command line argument of ld).
SO_VERSION=1

include $(MKFILES_ROOT)/qmacros.mk
ifndef QNX_INTERNAL
QNX_INTERNAL=$(PROJECT_ROOT)/.qnx_internal.mk
endif
include $(QNX_INTERNAL)

include $(MKFILES_ROOT)/qtargets.mk

OPTIMIZE_TYPE_g=none
OPTIMIZE_TYPE=$(OPTIMIZE_TYPE_$(filter g, $(VARIANTS)))

