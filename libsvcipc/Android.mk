LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_CFLAGS := \
	-D__QNX__

LOCAL_C_INCLUDES:= \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/include/svcipc \
	$(LOCAL_PATH)/include/sys \
	$(LOCAL_PATH)/src/commands \
	$(LOCAL_PATH)/src/core \
	$(LOCAL_PATH)/../../dbus
	
LOCAL_MODULE := libsvcipc

LOCAL_MODULE_CLASS := STATIC_LIBRARIES

LOCAL_SRC_FILES:= \
	src/commands/closeConnection.c \
        src/commands/emit.c \
        src/commands/freeReqContext.c \
        src/commands/getConnection.c \
        src/commands/invoke.c \
        src/commands/nameHasOwner.c \
        src/commands/openConnection.c \
        src/commands/registerService.c \
        src/commands/returnResult.c \
        src/commands/subscribe.c \
        src/commands/unregisterService.c \
        src/commands/unsubscribe.c \
	src/core/command.c \
   	src/core/connection.c \
    	src/core/context.c \
   	src/core/dispatcher.c \
   	src/core/registration.c \
    	src/core/subscription.c \
    	src/core/svcipc.c \
    	src/core/timeout.c \
	src/core/trace.c \
	src/core/utils.c \
	src/core/watch.c




LOCAL_SHARED_LIBRARIES := libdbus \
 
#LOCAL_STATIC_LIBRARIES := libgdbus

LOCAL_MODULE_TAGS := optional


include $(BUILD_STATIC_LIBRARY)

