#include <sys/neutrino.h>
#include <pthread.h>
#include <string.h>

#define TRUE
#define FALSE


#define Q_SIZE     (256)

uint8_t g_qFull;
uint8_t g_qEmpty;
static struct _pulse g_queuePulse [Q_SIZE];
static uint16_t g_readIndex;
static uint16_t g_writeIndex;
pthread_mutex_t g_mutex;
pthread_mutex_t g_signal_mutex;
pthread_cond_t g_signal;


int ChannelCreate( unsigned flags )
{
    g_qFull = SVCIPC_FALSE;
    g_qEmpty = SVCIPC_TRUE;

    g_readIndex = 0;
    g_writeIndex = 0;

    memset (&g_queuePulse[0], 0, sizeof (g_queuePulse));
    pthread_mutex_init (&g_mutex, NULL);
    pthread_mutex_init (&g_signal_mutex, NULL);
    pthread_cond_init (&g_signal, NULL);

    return 0;
}

int ConnectAttach( uint32_t nd,
                   pid_t pid,
                   int chid,
                   unsigned index,
                   int flags )
{
    return 1;
}

int MsgSendPulse ( int coid,
                   int priority,
                   int code,
                   int value )
{
    uint16_t readIndex;
    uint16_t writeIndex;

    pthread_mutex_lock(&g_mutex);

    readIndex = g_readIndex;
    writeIndex = g_writeIndex;

    if (g_qFull == SVCIPC_TRUE) {
        pthread_mutex_unlock(&g_mutex);
        return -1;
    }
    else {

        g_queuePulse [writeIndex].code = code;
        g_queuePulse [writeIndex].value.sival_int = value;

        writeIndex = (writeIndex + 1) % Q_SIZE;
        g_writeIndex = writeIndex;
        g_qEmpty = SVCIPC_FALSE;

        if (readIndex == writeIndex) {
            g_qFull = SVCIPC_TRUE;
        }

        pthread_mutex_lock(&g_signal_mutex);
        pthread_cond_signal(&g_signal);
        pthread_mutex_unlock(&g_signal_mutex);

        pthread_mutex_unlock(&g_mutex);

        return 0;
    }
}

int MsgReceivePulse( int chid,
                     void * data,
                     int bytes,
                     void *info )
{
    uint16_t readIndex;
    uint16_t writeIndex;
    struct _pulse *pulse_ptr = data;

    pthread_mutex_lock(&g_mutex);

    while (g_qEmpty == SVCIPC_TRUE) {

        /* release semaphore */
        pthread_mutex_unlock(&g_mutex);
        
        pthread_mutex_lock(&g_signal_mutex);
        pthread_cond_wait(&g_signal, &g_signal_mutex);
        pthread_mutex_unlock(&g_signal_mutex);

        pthread_mutex_lock(&g_mutex);
    }

    readIndex = g_readIndex;
    writeIndex = g_writeIndex;

    pulse_ptr->code = g_queuePulse [readIndex].code;
    pulse_ptr->value.sival_int = g_queuePulse [readIndex].value.sival_int;

    readIndex = (readIndex + 1) % Q_SIZE;

    g_readIndex = readIndex;
    g_qFull = SVCIPC_FALSE;

    if (readIndex == writeIndex) {
        g_qEmpty = SVCIPC_TRUE;
    }

    pthread_mutex_unlock(&g_mutex);

    return 0;
}
