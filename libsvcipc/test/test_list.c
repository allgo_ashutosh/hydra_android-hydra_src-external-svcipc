/*
 * list.c
 *
 *  Created on: Apr 5, 2012
 *      Author: AMckewan
 */

#include "list.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct myobj
{
   LIST_NODE(struct myobj);
   int data;
} myobj_t;

static myobj_t mylist = {&mylist, &mylist};

void verify()
{
   // make sure list is in the correct
   int total = 0;

   // iterate forwards, setting each node's data to the current total
   LIST_ITERATOR(myobj_t, fwd, mylist);
   while (!LIST_END(fwd, mylist))
   {
      total++;
      fwd->data = total;
      LIST_NEXT(fwd);
   }

   // now go backwards, verify each node has correct data value
   LIST_REVERSE_ITERATOR(myobj_t, rev, mylist);
   while (!LIST_END(rev, mylist))
   {
      if (rev->data != total)
      {
         printf("***** VERIFY ERROR: invalid node %p data=%d\n", rev, rev->data);
         return;
      }
      rev->data = 0;
      total--;
      LIST_PREV(rev);
   }

   if (total != 0)
      printf("***** VERIFY ERROR: total error %d\n", total);
}


static void showlist()
{
   verify();
   printf("   list %p: next=%p prev=%p\n", &mylist, mylist.next, mylist.prev);
   LIST_FOREACH(myobj_t, node, mylist)
   {
      printf("    node %p: next=%p prev=%p\n", node, node->next, node->prev);
   }
}

static void init()
{
   LIST_INIT(&mylist);
   showlist();
}

static void insert(myobj_t *obj)
{
   printf("insert %p\n", obj);
   LIST_INSERT(&mylist, obj);
   obj->data = 0;
   showlist();
}

static void append(myobj_t *obj)
{
   printf("append %p\n", obj);
   LIST_APPEND(&mylist, obj);
   obj->data = 0;
   showlist();
}

static void unsafe_remove(myobj_t *obj)
{
   printf("unsafe_remove %p\n", obj);
   LIST_REMOVE(obj);
   showlist();
}

static void safe_remove(myobj_t *obj)
{
   printf("safe_remove %p\n", obj);
   LIST_ITERATOR(myobj_t, node, mylist);
   while (!LIST_END(node, mylist))
   {
      if (node == obj)
      {
         LIST_REMOVE(obj);
         showlist();
         return;
      }
      LIST_NEXT(node);
   }
   printf("   node %p not found in the list\n", obj);
}

static void visit_all()
{
   myobj_t *node = mylist.next;
   while (node != &mylist)
   {
      // do something with node
      node = node->next;
   }
}

static int _contains(myobj_t *obj)
{
   LIST_CONTAINS_IMPL(myobj_t, &mylist, obj);
}

static int contains(myobj_t *obj)
{
   int found = _contains(obj);
   printf("list %s node %p\n", found?"contains":"does not contain", obj);
   return found;
}

void list_test()
{
   myobj_t o4, o3, o2, o1;
   printf("list test\n");
   init();

   insert(&o2);
   insert(&o1);
   append(&o3);

   contains(&o1);
   contains(&o2);
   contains(&o3);
   contains(&o4);
   contains(0);

   append(&o4);

   unsafe_remove(&o2);
   unsafe_remove(&o1);
   unsafe_remove(&o4);
   unsafe_remove(&o3);

   append(&o1);
   append(&o2);
   safe_remove(&o3);
   safe_remove(&o1);
   safe_remove(0);

   return;

   visit_all();
}




#if 0
int list_contains(list_node_t *list, list_node_t *node)
{
   list_node_t *n = list;
   while (n->next != list)
   {
      n = n->next;
      if (n == node)
         return 1;
   }
   return 0;
}

typedef struct node
{
   struct node *next;
   struct node *prev;

} list_node_t;

void list_init(list_node_t *list)
{
   list->next = list->prev = list;
}

int list_contains(list_node_t *list, list_node_t *node)
{
   list_node_t *n = list;
   while (n->next != list)
   {
      n = n->next;
      if (n == node)
         return 1;
   }
   return 0;
}

void list_insert(list_node_t *list, list_node_t *node)
{
   node->next = list->next;
   node->prev = list;
   list->next->prev = node;
   list->next = node;
}

void list_append(list_node_t *list, list_node_t *node)
{
   list_insert(list->prev, node);
}

list_node_t *list_remove(list_node_t *list, list_node_t *node)
{
   node->next->prev = node->prev;
   node->prev->next = node->next;
}
#endif
