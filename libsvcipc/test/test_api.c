/*
 * test_api.c
 *
 *  Created on: Apr 10); 2012
 *      Author: AMckewan
 */

#include "svcipc/svcipc.h"

// not called, just use to check implementations
void test_api(void)
{
   SVCIPC_asyncEmit(0,0,0,0,0);
   SVCIPC_asyncGetConnection(0,0,0,0);
   SVCIPC_asyncInvoke(0,0,0,0,0,0,0,0,0,0);
   SVCIPC_asyncNameHasOwner(0,0,0,0);
//   SVCIPC_asyncOpenConnection(0,0,0,0);
   SVCIPC_asyncRegisterService(0,0,0,0,0,0,0);
   SVCIPC_asyncReturnError(0,0,0,0,0);
   SVCIPC_asyncReturnResult(0,0,0,0);
   SVCIPC_asyncSubscribe(0,0,0,0,0,0);
   SVCIPC_asyncSubscribeOwnerChanged(0,0,0,0,0);
   SVCIPC_asyncUnregisterService(0,0,0);
   SVCIPC_asyncUnsubscribe(0,0,0);
//   SVCIPC_cancel(0);
   SVCIPC_closeConnection(0);
   SVCIPC_emit(0,0,0);
   SVCIPC_freeReqContext(0);
   SVCIPC_freeResponse(0);
   SVCIPC_getConnection(0,0,0);
   SVCIPC_initialize();
   SVCIPC_invoke(0,0,0,0,0,0,0);
   SVCIPC_nameHasOwner(0,0,0);
//   SVCIPC_openConnection(0,0,0);
   SVCIPC_registerService(0,0,0,0,0,0,0);
   SVCIPC_returnError(0,0,0);
   SVCIPC_returnResult(0,0);
   SVCIPC_shutdown();
   SVCIPC_subscribe(0,0,0,0,0,0);
   SVCIPC_subscribeOwnerChanged(0,0,0,0,0);
   SVCIPC_unregisterService(0);
   SVCIPC_unsubscribe(0);
}
