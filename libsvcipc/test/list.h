/*
 * list.h
 *
 *  Created on: Apr 5, 2012
 *      Author: AMckewan
 */

#ifndef LIST_H_
#define LIST_H_

/*
 * Macros to implement a doubly-linked list.
 */

// declare the next and previous fields (use inside the struct definition)
#define LIST_NODE(type) \
   type *next; \
   type *prev; \

// initialize an empty list (this is required before using the list or we will crash!!!)
#define LIST_INIT(list) \
   (list)->next = (list)->prev = (list)

// implementation of function to determine if the list contains a particular node
#define LIST_CONTAINS_IMPL(type, list, node) \
   type *n = (list); \
   while (n->next != (list)) { \
      n = n->next; \
      if (n == node) \
         return 1; \
   } return 0

// insert a node at the front of the list
#define LIST_INSERT(list, node) \
   do { \
      node->next = (list)->next; \
      node->prev = (list); \
      (list)->next->prev = node; \
      (list)->next = node; \
   } while (0)

// insert a node at the end of the list
#define LIST_APPEND(list, node) \
   do { \
      node->next = (list); \
      node->prev = (list)->prev; \
      (list)->prev->next = node; \
      (list)->prev = node; \
   } while (0)

// remove a node from the list (assumes it is in the list)
#define LIST_REMOVE(node) \
   do { \
      node->next->prev = node->prev; \
      node->prev->next = node->next; \
   } while (0)

/**
 * List iterators
 *
 * Use LIST_ITERATOR and LIST_NEXT to iterate forwards.
 * Use LIST_REVERSE_ITERATOR and LIST_PREV to iterate backwards.
 *
 * See test_list.c for examples.
 *
 * Note that unlike the macros above, this takes a list object rather than a pointer to the list.
 * This is just for readability to avoid some '&' in the code.
 */
#define LIST_ITERATOR(type, node, list)            type *node = (list).next
#define LIST_NEXT(node)                            node = node->next

#define LIST_REVERSE_ITERATOR(type, node, list)    type *node = (list).prev
#define LIST_PREV(node)                            node = node->prev

#define LIST_END(node, list)                       (node == &(list))

/*
 * Simplified macro for a loop to iterate over each element of the list.
 * This a combination of LIST_ITERATOR, LIST_END and LIST_NEXT above.
 *
 * LIST_FOREACH(mytype_t, node, mylist)
 * {
 *    myfunction(node);
 * }
 */

#define LIST_FOREACH(T, node, list) \
   T *node; \
   for (node = (list).next; node != &(list); node = node->next)


#endif /* LIST_H_ */
