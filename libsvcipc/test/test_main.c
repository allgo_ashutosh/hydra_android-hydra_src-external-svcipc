#include "test.h"
#include <semaphore.h>

void list_test();

static sem_t testSem;

//////////////////////////////////////////////////////////////////////
// Linux Assert functions



//////////////////////////////////////////////////////////////////////
// Test initialization

static void dummyGetConnectionCallback(const SVCIPC_tCallbackStatus* status, SVCIPC_tConnection conn,
      SVCIPC_tUserToken token)
{
   FAIL("dummyGetConnectionCallback");
}

static void dummyNameOwnerCallback(const SVCIPC_tCallbackStatus* status, SVCIPC_tConstStr busName,
      SVCIPC_tBool hasOwner, SVCIPC_tUserToken token)
{
   FAIL("dummyNameOwnerCallback");
}

static void testInitialize()
{
   SVCIPC_tConnection conn = 0;
   int hasOwner;

   // Before the call to SVCIPC_Initialize, all other functions should fail gracefully!
   ASSERT_ERROR(SVCIPC_asyncGetConnection(SVCIPC_CONNECTION_SESSION, 0, dummyGetConnectionCallback, 0), SVCIPC_ERR_BAD_ARGS);
   ASSERT_ERROR(SVCIPC_getConnection(SVCIPC_CONNECTION_SESSION, 0, &conn), SVCIPC_ERR_BAD_ARGS);

   ASSERT_ERROR(SVCIPC_asyncNameHasOwner(conn, TEST_BUS_NAME, dummyNameOwnerCallback, 0), SVCIPC_ERR_BAD_ARGS);
   ASSERT_ERROR(SVCIPC_nameHasOwner(conn, TEST_BUS_NAME, &hasOwner), SVCIPC_ERR_BAD_ARGS);

   ASSERT_OK(SVCIPC_initialize());
}

//////////////////////////////////////////////////////////////////////
// Connect test

/*
 static void onConnect(const SVCIPC_tCallbackStatus* status, SVCIPC_tConnection conn, SVCIPC_tUserToken token)
 {
 char buf[100];
 if (SVCIPC_IS_ERROR(status->errCode))
 {
 TRACE_INFO("onConnect errCode=%s, errName=%s errMsg=%s", decodeError(buf, status->errCode), status->errName, status->errMsg);
 }
 else
 {
 TRACE_INFO("onConnect conn=%p", conn);
 }
 }
 */

static void testGetConnection()
{
   SVCIPC_tConnection conn1, conn2, conn3;

   int i;
   for (i = 0; i < 10; i++)
   {
      conn1 = 0;
      ASSERT_OK(SVCIPC_getConnection(SVCIPC_CONNECTION_SESSION, 0, &conn1));
      ASSERT(conn1 != 0);
      ASSERT_OK(SVCIPC_closeConnection(conn1));
   }

   // try with bad args
   ASSERT_ERROR(SVCIPC_getConnection(5, 0, &conn1), SVCIPC_ERR_BAD_ARGS);
   ASSERT_ERROR(SVCIPC_getConnection(SVCIPC_CONNECTION_SESSION, 0, NULL), SVCIPC_ERR_BAD_ARGS);

   // open 3 connections
   conn1 = conn2 = conn3 = 0;
   ASSERT_OK(SVCIPC_getConnection(SVCIPC_CONNECTION_SESSION, 0, &conn1));
   ASSERT(conn1 != 0);
   ASSERT_OK(SVCIPC_getConnection(SVCIPC_CONNECTION_SESSION, 0, &conn2));
   ASSERT(conn2 != 0);
   ASSERT_OK(SVCIPC_getConnection(SVCIPC_CONNECTION_SESSION, 0, &conn3));
   ASSERT(conn3 != 0);

   // close connections
   ASSERT_OK(SVCIPC_closeConnection(conn1));
   ASSERT_OK(SVCIPC_closeConnection(conn2));
   ASSERT_OK(SVCIPC_closeConnection(conn3));

   // close invalid connections
   ASSERT_ERROR(SVCIPC_closeConnection(conn1), SVCIPC_ERR_BAD_ARGS);
   ASSERT_ERROR(SVCIPC_closeConnection(0), SVCIPC_ERR_BAD_ARGS);
   ASSERT_ERROR(SVCIPC_closeConnection((SVCIPC_tConnection) -1), SVCIPC_ERR_BAD_ARGS);
}

//////////////////////////////////////////////////////////////////////

static void nameOwnerCallback(const SVCIPC_tCallbackStatus* status, SVCIPC_tConstStr busName, SVCIPC_tBool hasOwner,
      SVCIPC_tUserToken token)
{
   if (!strcmp(busName, UNKNOWN_BUS_NAME))
   {
      ASSERT_MSG(!hasOwner, "asyncNameHasOwner " UNKNOWN_BUS_NAME);
   }
   else
   {
      ASSERT_MSG(hasOwner, "asyncNameHasOwner " TEST_BUS_NAME);
   }
}

static void testNameHasOwner(SVCIPC_tConnection conn)
{
   int hasOwner = 0;
   ASSERT_ERROR(SVCIPC_nameHasOwner(conn, UNKNOWN_BUS_NAME, NULL), SVCIPC_ERR_BAD_ARGS);

   ASSERT_OK(SVCIPC_nameHasOwner(conn, UNKNOWN_BUS_NAME, &hasOwner));
   ASSERT_MSG(!hasOwner, "nameHasOwner " UNKNOWN_BUS_NAME);

   ASSERT_OK(SVCIPC_nameHasOwner(conn, TEST_BUS_NAME, &hasOwner));
   ASSERT_MSG(hasOwner, "nameHasOwner " TEST_BUS_NAME);
}

static void testAsyncNameHasOwner(SVCIPC_tConnection conn)
{
   ASSERT_OK(SVCIPC_asyncNameHasOwner(conn, UNKNOWN_BUS_NAME, nameOwnerCallback, 0));
   ASSERT_OK(SVCIPC_asyncNameHasOwner(conn, TEST_BUS_NAME, nameOwnerCallback, 0));
}

//////////////////////////////////////////////////////////////////////
// test invoke

static const char echoArgs[] = "Is there an echo in here?";
static char invokeToken[1];

static void testInvoke(SVCIPC_tConnection conn)
{
   SVCIPC_tResponse *resp = 0;

   // good method
   ASSERT_OK(SVCIPC_invoke(conn, TEST_BUS_NAME, 0, "echo", echoArgs, 1000, &resp));
   ASSERT(resp != 0);
   if (resp)
   {
      ASSERT_OK(resp->status.errCode);
      ASSERT(resp->result && !strcmp(resp->result, echoArgs));
      SVCIPC_freeResponse(resp);
   }

   // unknown bus name
   resp = 0;
   ASSERT_ERROR(SVCIPC_invoke(conn, UNKNOWN_BUS_NAME, 0, "anyMethod", "{}", 1000, &resp), SVCIPC_ERR_DBUS);
   ASSERT(resp != 0);
   if (resp)
   {
      ASSERT_ERROR(resp->status.errCode, SVCIPC_ERR_DBUS);
      ASSERT(resp->status.errName && !strcmp(resp->status.errName, "org.freedesktop.DBus.Error.ServiceUnknown"));
      ASSERT(resp->status.errMsg && !strcmp(resp->status.errMsg, "The name "UNKNOWN_BUS_NAME" was not provided by any .service files"));
      SVCIPC_freeResponse(resp);
   }

   // unknown method
   resp = 0;
   ASSERT_ERROR(SVCIPC_invoke(conn, TEST_BUS_NAME, 0, "unknownMethod", "{}", 1000, &resp), SVCIPC_ERR_DBUS);
   ASSERT(resp != 0);
   if (resp)
   {
      ASSERT_ERROR(resp->status.errCode, SVCIPC_ERR_DBUS);
      ASSERT(resp->status.errName && !strcmp(resp->status.errName, "com.harman.service.Error"));
      ASSERT(resp->status.errMsg && !strcmp(resp->status.errMsg, "unknownMethod not implemented"));
      SVCIPC_freeResponse(resp);
   }

   // long method, this one should not time out
   resp = 0;
   ASSERT_OK(SVCIPC_invoke(conn, TEST_BUS_NAME, 0, "sleep", "100", 500, &resp));
   ASSERT(resp != 0);
   SVCIPC_freeResponse(resp);

   // this one should time out
   resp = 0;
   ASSERT_ERROR(SVCIPC_invoke(conn, TEST_BUS_NAME, 0, "sleep", "300", 100, &resp), SVCIPC_ERR_DBUS);
   ASSERT(resp != 0);
   if (resp)
   {
      ASSERT_ERROR(resp->status.errCode, SVCIPC_ERR_DBUS);
      ASSERT(resp->status.errName && !strcmp(resp->status.errName, "org.freedesktop.DBus.Error.NoReply"));
      ASSERT(resp->status.errMsg != 0);
      SVCIPC_freeResponse(resp);
   }

   // *** The last method timed out, but the service is still executing it.
   // *** Make sure the timeout of the next invoke is long enough!

   // method that returns an error
   resp = 0;
   ASSERT_ERROR(SVCIPC_invoke(conn, TEST_BUS_NAME, 0, "fail", NULL, 1000, &resp), SVCIPC_ERR_DBUS);
   //showResponse(resp);
   ASSERT(resp != 0);
   if (resp)
   {
      ASSERT_ERROR(resp->status.errCode, SVCIPC_ERR_DBUS);
      ASSERT(resp->status.errName && !strcmp(resp->status.errName, "com.harman.service.Error"));
      ASSERT(resp->status.errMsg && !strcmp(resp->status.errMsg, "This method failed"));
      SVCIPC_freeResponse(resp);
   }

}

//////////////////////////////////////////////////////////////////////
// test async invoke

static void unknownBusNameResult(const SVCIPC_tCallbackStatus* status, SVCIPC_tConstStr result, SVCIPC_tUserToken token)
{
   ASSERT(status && status->errMsg && status->errName);
   ASSERT_ERROR(status->errCode, SVCIPC_ERR_DBUS);
   ASSERT(!strcmp(status->errName, "org.freedesktop.DBus.Error.ServiceUnknown"));
   ASSERT(!strcmp(status->errMsg, "The name "UNKNOWN_BUS_NAME" was not provided by any .service files"));
   ASSERT(token == invokeToken);
}

static void unknownMethodResult(const SVCIPC_tCallbackStatus* status, SVCIPC_tConstStr result, SVCIPC_tUserToken token)
{
   ASSERT(status && status->errMsg && status->errName);
   ASSERT_ERROR(status->errCode, SVCIPC_ERR_DBUS);
   //printf("errName=%s\nerrMsg=%s\n", status->errName, status->errMsg);
   ASSERT(!strcmp(status->errName, "com.harman.service.Error"));
   ASSERT(!strcmp(status->errMsg, "unknownMethod not implemented"));
   ASSERT(token == invokeToken);
}

static void echoResult(const SVCIPC_tCallbackStatus* status, SVCIPC_tConstStr result, SVCIPC_tUserToken token)
{
   ASSERT_OK(status->errCode);
   ASSERT(result && !strcmp(result, echoArgs));
   ASSERT(token == invokeToken);
}

static void failResult(const SVCIPC_tCallbackStatus* status, SVCIPC_tConstStr result, SVCIPC_tUserToken token)
{
   ASSERT(status && status->errMsg && status->errName);
   ASSERT_ERROR(status->errCode, SVCIPC_ERR_DBUS);
   //printf("errName=%s\nerrMsg=%s\n", status->errName, status->errMsg);
   ASSERT(!strcmp(status->errName, "com.harman.service.Error"));
   ASSERT(!strcmp(status->errMsg, "This method failed"));
   ASSERT(token == invokeToken);
}

static void testAsyncInvoke(SVCIPC_tConnection conn)
{
   ASSERT_OK(SVCIPC_asyncInvoke(conn, UNKNOWN_BUS_NAME, 0, "anyMethod", "{}", 0, 1000, unknownBusNameResult, NULL, invokeToken));
   ASSERT_OK(SVCIPC_asyncInvoke(conn, TEST_BUS_NAME, 0, "unknownMethod", "{}", 0, 1000, unknownMethodResult, NULL, invokeToken));
   ASSERT_OK(SVCIPC_asyncInvoke(conn, TEST_BUS_NAME, 0, "echo", echoArgs, 0, 1000, echoResult, NULL, invokeToken));
   ASSERT_OK(SVCIPC_asyncInvoke(conn, TEST_BUS_NAME, 0, "fail", NULL, 0, 1000, failResult, NULL, invokeToken));
}

//////////////////////////////////////////////////////////////////////
// Test owner changed subscription

static SVCIPC_tSigSubHnd testOwnerChangedSubHnd;
static SVCIPC_tSigSubHnd anyOwnerChangedSubHnd;
static SVCIPC_tSigSubHnd asyncOwnerChangedSubHnd;
static int testServiceRunning;
static int ownerChangedCallbacks;
static char ownerChangedToken[1];

static void testOwnerChanged(SVCIPC_tConstStr newName, SVCIPC_tConstStr oldOwner, SVCIPC_tConstStr newOwner,
      SVCIPC_tUserToken token)
{
   ASSERT(newName && oldOwner && newOwner);
   ASSERT(!strcmp(newName, TEST_BUS_NAME));
   ASSERT(token == ownerChangedToken);
   if (testServiceRunning)
      ASSERT(!*oldOwner && *newOwner);
   else
      ASSERT(*oldOwner && !*newOwner);
   ownerChangedCallbacks++;

   /*
    if (!*oldOwner)
    oldOwner = "[none]";
    if (!*newOwner)
    newOwner = "[none]";
    printf("testOwnerChanged newName=%s oldOwner=%s newOwner=%s\n", newName, oldOwner, newOwner);
    */

}

static void anyOwnerChanged(SVCIPC_tConstStr newName, SVCIPC_tConstStr oldOwner, SVCIPC_tConstStr newOwner,
      SVCIPC_tUserToken token)
{
   ASSERT(newName && oldOwner && newOwner);
   ASSERT(token == ownerChangedToken);
   if (!strcmp(newName, TEST_BUS_NAME))
   {
      if (testServiceRunning)
         ASSERT(!*oldOwner && *newOwner);
      else
         ASSERT(*oldOwner && !*newOwner);
      ownerChangedCallbacks++;
   }

   /*
    if (!*oldOwner)
    oldOwner = "[none]";
    if (!*newOwner)
    newOwner = "[none]";
    printf("anyOwnerChanged newName=%s oldOwner=%s newOwner=%s\n", newName, oldOwner, newOwner);
    */
}

static void ownerSubscriptionCallback(const SVCIPC_tCallbackStatus* status, SVCIPC_tSigSubHnd subHnd, SVCIPC_tUserToken token)
{
   ASSERT_OK(status->errCode);
   ASSERT(token == ownerChangedToken);
   asyncOwnerChangedSubHnd = subHnd;
}

static void testSubscribeOwnerChanged(SVCIPC_tConnection conn)
{
   testServiceRunning = 0;
   ownerChangedCallbacks = 0;

   // subscribe for test bus and any bus
   ASSERT_OK(SVCIPC_subscribeOwnerChanged(conn, TEST_BUS_NAME, testOwnerChanged, ownerChangedToken, &testOwnerChangedSubHnd));
   ASSERT_OK(SVCIPC_subscribeOwnerChanged(conn, NULL, anyOwnerChanged, ownerChangedToken, &anyOwnerChangedSubHnd));
   ASSERT_OK(SVCIPC_asyncSubscribeOwnerChanged(conn, NULL, anyOwnerChanged, ownerSubscriptionCallback, ownerChangedToken));

   // start/stop test sevice while processing signals
   testServiceRunning = 1;
   startTestService(conn);
   delay(100);

   testServiceRunning = 0;
   stopTestService();
   delay(100);

   // unsubscribe
   ASSERT_OK(SVCIPC_unsubscribe(testOwnerChangedSubHnd));
   ASSERT_OK(SVCIPC_unsubscribe(anyOwnerChangedSubHnd));
   ASSERT_OK(SVCIPC_unsubscribe(asyncOwnerChangedSubHnd));

   testServiceRunning = 1;
   startTestService(conn);
   delay(100);

   stopTestService();
   delay(100);

   ASSERT(ownerChangedCallbacks == 6);
}

//////////////////////////////////////////////////////////////////////
// Test signals

static SVCIPC_tSigSubHnd pingSubHnd;
static char pingToken[1];
static int pings;

static void onPing(SVCIPC_tConstStr sigName, SVCIPC_tConstStr data, SVCIPC_tUserToken token)
{
   //printf("onPing\n");
   ASSERT(!strcmp(sigName, "ping"));
   ASSERT(!strcmp(data, "{}"));
   ASSERT(token == pingToken);
   pings++;
}

static void testSubscribe(SVCIPC_tConnection conn)
{
   const int pingsExpected = 3;
   char args[100];
   sprintf(args, "%d", pingsExpected);
   pings = 0;

   // try with invalid args
   static SVCIPC_tSigSubHnd dummySubHnd;
   ASSERT_ERROR(SVCIPC_subscribe(conn, TEST_OBJ_PATH, "ping", NULL, pingToken, &dummySubHnd), SVCIPC_ERR_BAD_ARGS);
   ASSERT_ERROR(SVCIPC_subscribe(conn, TEST_OBJ_PATH, "ping", onPing, pingToken, NULL), SVCIPC_ERR_BAD_ARGS);

   // subscribe to ping signal
   ASSERT_OK(SVCIPC_subscribe(conn, TEST_OBJ_PATH, "ping", onPing, pingToken, &pingSubHnd));

   // stimulate some pings
   SVCIPC_tResponse *response;
   ASSERT_OK(SVCIPC_invoke(conn, TEST_BUS_NAME, 0, "ping", args, 5000, &response));
   SVCIPC_freeResponse(response);
   delay(100);

   // unsubscribe, we should get no more pings
   ASSERT_OK(SVCIPC_unsubscribe(pingSubHnd));

   ASSERT_OK(SVCIPC_invoke(conn, TEST_BUS_NAME, 0, "ping", args, 5000, &response));
   SVCIPC_freeResponse(response);
   delay(100);

   //printf("pings=%d expected=%d\n", pings, pingsExpected);
   ASSERT(pings == pingsExpected);
}

//////////////////////////////////////////////////////////////////////
// Test async subscribe

static void subscribeCallback(const SVCIPC_tCallbackStatus* status, SVCIPC_tSigSubHnd subHnd, SVCIPC_tUserToken token)
{
   ASSERT_OK(status->errCode);
   ASSERT(token == pingToken);
   pingSubHnd = subHnd;
   sem_post(&testSem);
}

static void unsubscribeCallback(const SVCIPC_tCallbackStatus* status, SVCIPC_tUserToken token)
{
   ASSERT_OK(status->errCode);
   ASSERT(token == pingToken);
   pingSubHnd = 0;
   sem_post(&testSem);
}

static void testAsyncSubscribe(SVCIPC_tConnection conn)
{
   pingSubHnd = 0;
   ASSERT_OK(SVCIPC_asyncSubscribe(conn, TEST_OBJ_PATH, "ping", onPing, subscribeCallback, pingToken));
   sem_wait(&testSem);
   ASSERT(pingSubHnd != 0);
   ASSERT_OK(SVCIPC_asyncUnsubscribe(pingSubHnd, unsubscribeCallback, pingToken));
   sem_wait(&testSem);
   ASSERT(pingSubHnd == 0);
}

//////////////////////////////////////////////////////////////////////
// Test register service

static char requestToken[1];

static void onRequest(SVCIPC_tReqContext context, SVCIPC_tConstStr method, SVCIPC_tConstStr parms,
      SVCIPC_tBool noReplyExpected, SVCIPC_tUserToken token)
{
   ASSERT(token == requestToken);
}

static void testRegisterService(SVCIPC_tConnection conn)
{
   SVCIPC_tSvcRegHnd regHnd, regHnd1, regHnd2;

   // standard call
   ASSERT_OK(SVCIPC_registerService(conn, TEST_BUS_NAME, TEST_OBJ_PATH, 0, onRequest, requestToken, &regHnd));
   ASSERT_OK(SVCIPC_unregisterService(regHnd));

   // default obj path
   ASSERT_OK(SVCIPC_registerService(conn, TEST_BUS_NAME, NULL, 0, onRequest, requestToken, &regHnd));
   ASSERT_OK(SVCIPC_unregisterService(regHnd));

   // Invalid names
   ASSERT_ERROR(SVCIPC_registerService(conn, "???", "???", 0, onRequest, requestToken, &regHnd), SVCIPC_ERR_DBUS);

   // null reg hnd
   ASSERT_ERROR(SVCIPC_registerService(conn, TEST_BUS_NAME, TEST_OBJ_PATH, 0, onRequest, requestToken, NULL), SVCIPC_ERR_BAD_ARGS);
   ASSERT_ERROR(SVCIPC_unregisterService(NULL), SVCIPC_ERR_BAD_ARGS);

   // try to unregister a service twice
   ASSERT_OK(SVCIPC_registerService(conn, TEST_BUS_NAME, TEST_OBJ_PATH, 0, onRequest, requestToken, &regHnd));
   ASSERT_OK(SVCIPC_unregisterService(regHnd));
   ASSERT_ERROR(SVCIPC_unregisterService(regHnd), SVCIPC_ERR_BAD_ARGS);

   // try to register the same service twice
   // currently what happens is the second registration returns "you already have it" which we treat as success.
   // then the first unregister unregisters the name (meaning we won't get requests any more nor can we send signals),
   // the second one fails (we don't own the name) but we treat that as success.
   // This is consistent with the original implementation but probably not correct. It should either fail the second
   // request or return the same request object and increment a reference count.
   //svcipcTraceLevel = TRACE_LEVEL_INFO;
   ASSERT_OK(SVCIPC_registerService(conn, TEST_BUS_NAME, TEST_OBJ_PATH, 0, onRequest, requestToken, &regHnd1));
   ASSERT_OK(SVCIPC_registerService(conn, TEST_BUS_NAME, TEST_OBJ_PATH, 0, onRequest, requestToken, &regHnd2));
   ASSERT_OK(SVCIPC_unregisterService(regHnd1));
   ASSERT_OK(SVCIPC_unregisterService(regHnd2));
   //svcipcTraceLevel = TRACE_LEVEL_NONE;
}

static SVCIPC_tSvcRegHnd asyncRegHnd;

static void onRegister(const SVCIPC_tCallbackStatus* status, SVCIPC_tSvcRegHnd regHnd, SVCIPC_tUserToken token)
{
   ASSERT(status != 0);
   if (status)
   {
      ASSERT_ERROR(status->errCode, (SVCIPC_tError) token);
      if (!SVCIPC_IS_ERROR(status->errCode))
      {
         asyncRegHnd = regHnd;
      }
   }
   sem_post(&testSem);
}

static void onUnregisterStatus(const SVCIPC_tCallbackStatus* status, SVCIPC_tUserToken token)
{
   ASSERT(status != 0);
   if (status)
   {
      ASSERT_ERROR(status->errCode, (SVCIPC_tError) token);
      if (!SVCIPC_IS_ERROR(status->errCode))
      {
         asyncRegHnd = 0;
      }
   }
   sem_post(&testSem);
}

static void testAsyncRegisterService(SVCIPC_tConnection conn)
{
   asyncRegHnd = 0;
   ASSERT_OK(SVCIPC_asyncRegisterService( conn, TEST_BUS_NAME, TEST_OBJ_PATH, 0, onRequest, onRegister, (SVCIPC_tUserToken)SVCIPC_ERR_OK));
   sem_wait(&testSem);
   ASSERT(asyncRegHnd != 0);

   ASSERT_OK(SVCIPC_asyncUnregisterService(asyncRegHnd, onUnregisterStatus, (SVCIPC_tUserToken)SVCIPC_ERR_OK));
   sem_wait(&testSem);
   ASSERT(asyncRegHnd == 0);

   // invalid characters in name
   asyncRegHnd = 0;
   ASSERT_OK(SVCIPC_asyncRegisterService( conn, "???", "???", 0, onRequest, onRegister, (SVCIPC_tUserToken)SVCIPC_ERR_DBUS));
   sem_wait(&testSem);
   ASSERT(asyncRegHnd == 0);
}

/*
 SVCIPC_tError
 */

//////////////////////////////////////////////////////////////////////
// Test main

int main(int argc, char *argv[])
{
   sem_init(&testSem, 0, 0);
   testStart("svcipc");

   TEST(testInitialize());
   delay (200); 

   TEST(testGetConnection());

   SVCIPC_tConnection conn = 0;
   ASSERT_OK(SVCIPC_getConnection(SVCIPC_CONNECTION_SESSION, 0, &conn));

   if (conn)
   {
      TEST(testRegisterService(conn));
      TEST(testAsyncRegisterService(conn));
      TEST(testSubscribeOwnerChanged(conn)); // includes async

      TEST(startTestService(conn));

      TEST(testNameHasOwner(conn));
      TEST(testAsyncNameHasOwner(conn));
      TEST(testInvoke(conn));
      TEST(testAsyncInvoke(conn));
      TEST(testSubscribe(conn));
      TEST(testAsyncSubscribe(conn));
   }

#if 0
   return 0;

   //   svcipcTraceLevel = TRACE_LEVEL_INFO;

   //   list_test();
   //   return 0;

   goto end;

#endif

   //   delay(500);
   SVCIPC_shutdown();
   //   delay(500);
   sem_destroy(&testSem);
   return testComplete();
}
