-- svcipc test

require "service"

name = "com.harman.service.Test"
methods = {}
myservice = service.register(name, methods)

function methods.hello(args)
   return {result="hello to you!"}
end

function methods.sleep(args)
   local msec = args.msec or 5
   print("delaying for "..msec.." msec")
   os.sleep(msec)
   return {slept=msec}
end
