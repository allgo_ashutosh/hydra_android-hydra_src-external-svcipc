// Simple unit test

#include "test.h"

static int passed, failed;
static uint64_t startTime;

void testStart(const char *msg)
{
   printf("Test 0.000 start\n");
   passed = failed = 0;
   startTime = svcipcGetSystemTime();
}

int testComplete(void)
{
   uint32_t t = (uint32_t)(svcipcGetSystemTime() - startTime);
   printf("Test %u.%03u complete, passed = %d, failed = %d\n", t/1000, t%1000, passed, failed);
   return failed;
}

void showTest(const char *msg)
{
   uint32_t t = (uint32_t)(svcipcGetSystemTime() - startTime);
   printf("Test %u.%03u %s ...\n", t/1000, t%1000, msg);
}

void testAssert(int result, const char *msg, const char *file, int line)
{
   if (result)
   {
      passed++;
   }
   else
   {
      printf("  %s:%d FAIL: %s\n", file, line, msg);
      failed++;
   }
}

void testAssertError(int result, int expected, const char *msg, const char *file, int line)
{
   if (result == expected)
   {
      passed++;
   }
   else
   {
      printf("  %s:%d FAIL: %s : expected %s got %s\n", file, line, msg, errmsg(expected), errmsg(result));
      failed++;
   }
}

//////////////////////////////////////////////////////////////////////

#include "svcipc/svcipc.h"

char *errmsg(SVCIPC_tError error)
{
   switch (SVCIPC_ERR_GET_CODE(error))
   {
   case SVCIPC_ERR_OK:
      return "SVCIPC_ERR_OK";
   case SVCIPC_ERR_NOT_SUPPORTED:
      return "SVCIPC_ERR_NOT_SUPPORTED";
   case SVCIPC_ERR_NO_MEMORY:
      return "SVCIPC_ERR_NO_MEMORY";
   case SVCIPC_ERR_BAD_ARGS:
      return "SVCIPC_ERR_BAD_ARGS";
   case SVCIPC_ERR_INTERNAL:
      return "SVCIPC_ERR_INTERNAL";
   case SVCIPC_ERR_DBUS:
      return "SVCIPC_ERR_DBUS";
   case SVCIPC_ERR_CMD_SUBMISSION:
      return "SVCIPC_ERR_CMD_SUBMISSION";
   case SVCIPC_ERR_NOT_CONNECTED:
      return "SVCIPC_ERR_NOT_CONNECTED";
   case SVCIPC_ERR_CANCELLED:
      return "SVCIPC_ERR_CANCELLED";
   case SVCIPC_ERR_CONN_SEND:
      return "SVCIPC_ERR_CONN_SEND";
   case SVCIPC_ERR_NOT_FOUND:
      return "SVCIPC_ERR_NOT_FOUND";
   case SVCIPC_ERR_DEADLOCK:
      return "SVCIPC_ERR_DEADLOCK";
   default:
      return "SVCIPC_ERR_UNKNOWN";
   }
}

char *decodeError(char *buf, SVCIPC_tError error)
{
   sprintf(buf, "error=%x(%d,%d,%d) %s", error, SVCIPC_ERR_GET_LEVEL(error), SVCIPC_ERR_GET_DOMAIN(error),
         SVCIPC_ERR_GET_CODE(error), errmsg(error));
   return buf;
}

void showResponse(const SVCIPC_tResponse *response)
{
   if (response)
   {
      char buf[100];
      if (SVCIPC_IS_ERROR(response->status.errCode))
      {
         printf("invoke response: error %s, errName=%s errMsg=%s\n", decodeError(buf, response->status.errCode), response->status.errName, response->status.errMsg);
      }
      else
      {
         printf("invoke response: result %s\n", response->result);
      }
   }
   else
   {
      printf("invoke response is NULL\n");
   }
}

