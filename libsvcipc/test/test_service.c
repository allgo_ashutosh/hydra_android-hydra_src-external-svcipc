/*
 * test_service.c
 *
 *  Created on: Apr 7, 2012
 *      Author: AMckewan
 *
 */

#include "test.h"
#include <sys/neutrino.h>
#include <errno.h>

static SVCIPC_tSvcRegHnd regHnd;

//////////////////////////////////////////////////////////////////////
// Methods

static void echo(const char *args, int replyExpected, SVCIPC_tReqContext context)
{
   //TRACE_TEST("echo");
   if (replyExpected)
      ASSERT_OK(SVCIPC_returnResult(context, args));
   SVCIPC_freeReqContext(context);
}

static void echoAsync(const char *args, int replyExpected, SVCIPC_tReqContext context)
{
   if (replyExpected)
      ASSERT_OK(SVCIPC_asyncReturnResult(context, args, 0, 0));
   SVCIPC_freeReqContext(context);
}

static void fail(const char *args, int replyExpected, SVCIPC_tReqContext context)
{
   //TRACE_TEST("fail");
   ASSERT_OK(SVCIPC_returnError(context, 0, "This method failed"));
   SVCIPC_freeReqContext(context);
}

static void failAsync(const char *args, int replyExpected, SVCIPC_tReqContext context)
{
   ASSERT_OK(SVCIPC_asyncReturnError(context, 0, "This method failed", 0, 0));
   SVCIPC_freeReqContext(context);
}

static void ping(const char *args, int replyExpected, SVCIPC_tReqContext context)
{
   int i, count = atoi(args);
   if (replyExpected)
      ASSERT_OK(SVCIPC_returnResult(context, args));
   SVCIPC_freeReqContext(context);

   for (i = 0; i < count; i++)
   {
      //      ASSERT_OK(SVCIPC_emit(regHnd, "ping", "{}"));
      ASSERT_OK(SVCIPC_asyncEmit(regHnd, "ping", "{}", 0, 0));
   }
}

static void mysleep(const char *args, int replyExpected, SVCIPC_tReqContext context)
{
   int msec = atoi(args);
   delay(msec);
   if (replyExpected)
      ASSERT_OK(SVCIPC_returnResult(context, args));
   SVCIPC_freeReqContext(context);
}

static void callMethod(const char *method, const char *args, int replyExpected, SVCIPC_tReqContext context)
{
   if (!strcmp(method, "echo"))
      echo(args, replyExpected, context);
   else if (!strcmp(method, "echoAsync"))
      echoAsync(args, replyExpected, context);
   else if (!strcmp(method, "fail"))
      fail(args, replyExpected, context);
   else if (!strcmp(method, "failAsync"))
      failAsync(args, replyExpected, context);
   else if (!strcmp(method, "ping"))
      ping(args, replyExpected, context);
   else if (!strcmp(method, "sleep"))
      mysleep(args, replyExpected, context);
   else
   {
      char buf[100];
      sprintf(buf, "%s not implemented", method);
      ASSERT_OK(SVCIPC_returnError(context, 0, buf));
      SVCIPC_freeReqContext(context);
   }
}

//////////////////////////////////////////////////////////////////////
// Service thread

enum
{
   PULSE_QUIT = _PULSE_CODE_MINAVAIL, PULSE_METHOD,
};

static int coid, chid;
static pthread_t tid;

static int send(int code, void *value)
{
   return MsgSendPulse(coid, -1, code, (int) value);
}

struct request
{
   SVCIPC_tReqContext context;
   char *method;
   char *args;
   int noReplyExpected;
   void *token;
};

static void onRequest(SVCIPC_tReqContext context, SVCIPC_tConstStr method, SVCIPC_tConstStr parms,
      SVCIPC_tBool noReplyExpected, SVCIPC_tUserToken token)
{
   struct request *request = malloc(sizeof(struct request));
   request->context = context;
   request->method = strdup(method);
   request->args = strdup(parms);
   request->noReplyExpected = noReplyExpected;
   request->token = token;

   send(PULSE_METHOD, request);
}

static void handleRequest(struct request *req)
{
   //printf("method=%s args=%s\n", req->method, req->args);
   callMethod(req->method, req->args, !req->noReplyExpected, req->context);
   free(req->method);
   free(req->args);
   free(req);
}

static void *serviceThread(void *conn)
{
   for (;;)
   {
      struct _pulse pulse;
      if (MsgReceivePulse(chid, &pulse, sizeof(pulse), 0) == 0)
      {
         void *value = pulse.value.sival_ptr;
         //printf("received pulse %d %p\n", pulse.code, pulse.value.sival_ptr);
         if (pulse.code == PULSE_QUIT)
            break;
         else if (pulse.code == PULSE_METHOD)
            handleRequest(value);
      }
   }
   return 0;
}

void startTestService(SVCIPC_tConnection conn)
{
   //   ASSERT_OK(SVCIPC_initialize());
   //   ASSERT_OK(SVCIPC_getConnection(SVCIPC_CONNECTION_SESSION, 0, &conn));

   ASSERT_OK(SVCIPC_registerService(conn, TEST_BUS_NAME, 0, 0, onRequest, 0, &regHnd));

   chid = ChannelCreate(0);
   coid = ConnectAttach(0, 0, chid, _NTO_SIDE_CHANNEL, 0);

   pthread_attr_t attr;
   pthread_attr_init(&attr);
   //pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
   ASSERT(pthread_create(&tid, &attr, serviceThread, conn) == EOK);
}

void stopTestService(void)
{
   send(PULSE_QUIT, 0);
   ASSERT(pthread_join(tid, NULL) == EOK);
   ASSERT_OK(SVCIPC_unregisterService(regHnd));
}
