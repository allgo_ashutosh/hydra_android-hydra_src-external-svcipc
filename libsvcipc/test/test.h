/*
 * test.h
 *
 *  Created on: Apr 7, 2012
 *      Author: AMckewan
 */

#ifndef TEST_H_
#define TEST_H_

#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/poll.h>
#include <unistd.h>

#include "svcipc/svcipc.h"

// cheaters from the svcipc library
uint64_t svcipcGetSystemTime(void);
void svcipcTraceInit(int output, int level);
extern int svcipcTraceLevel;
#define TRACE_OUTPUT_SLOG     1
#define TRACE_OUTPUT_CONSOLE  2
#define TRACE_LEVEL_NONE      0
#define TRACE_LEVEL_ERROR     1
#define TRACE_LEVEL_WARN      2
#define TRACE_LEVEL_INFO      3
#define TRACE_LEVEL_VERBOSE   4


///////////////////////////////////////////////////////////////////////////
// Test macros

#define TEST(exp) showTest(#exp); exp

#define ASSERT(exp) testAssert(exp, #exp, __FILE__, __LINE__)
#define ASSERT_MSG(exp, msg) testAssert(exp, msg, __FILE__, __LINE__)
#define PASS(msg) testAssert(1, #msg, __FILE__, __LINE__)
#define FAIL(msg) testAssert(0, #msg, __FILE__, __LINE__)

void testStart(const char *msg);
int testComplete(void);
void showTest(const char *msg);
void testAssert(int result, const char *msg, const char *file, int line);

// asserts for svcipc api calls
#define ASSERT_ERROR(exp, errCode) testAssertError(SVCIPC_ERR_GET_CODE(exp), errCode, #exp, __FILE__, __LINE__)
#define ASSERT_OK(exp) ASSERT_ERROR(exp, SVCIPC_ERR_OK)

void testAssertError(int result, int expected, const char *msg, const char *file, int line);

///////////////////////////////////////////////////////////////////////////

#define TEST_BUS_NAME      "com.harman.service.Test"
#define TEST_OBJ_PATH      "/com/harman/service/Test"
#define UNKNOWN_BUS_NAME   "com.harman.service.Invalid"
#define INVALID_OBJ_PATH   "/com/harman/service/Invalid"

char *errmsg(SVCIPC_tError error);
char *decodeError(char *buf, SVCIPC_tError error);
void showResponse(const SVCIPC_tResponse *response);

void startTestService(SVCIPC_tConnection conn);
void stopTestService(void);


#endif /* TEST_H_ */
