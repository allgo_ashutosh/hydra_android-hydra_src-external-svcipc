#include <assert.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include "svcipc/svcipc.h"
#define false 0
#define true 1
static int gQuit=0;

static const int DEFAULT_REQ_TIMEOUT = 500;

void sigHandler(int siig)
{
   gQuit = 1;
}

void onSignal(SVCIPC_tConstStr  sigName,SVCIPC_tConstStr  data,SVCIPC_tUserToken token)
{
   SVCIPC_tConstStr subscriber = (SVCIPC_tConstStr)(token);
   printf("[%s] Signal : Data:  \n",subscriber,sigName,data);
}

void onNotifyAllOwnerChange(SVCIPC_tConstStr  newName,SVCIPC_tConstStr  oldOwner,SVCIPC_tConstStr  newOwner,SVCIPC_tUserToken token )
{
    printf("[onOwnerChange] newName: %s  oldOwner: %s  newOwner: %s  \n",newName,oldOwner,newOwner);
}


void onNotifyMediaOwnerChange(SVCIPC_tConstStr  newName,SVCIPC_tConstStr  oldOwner,SVCIPC_tConstStr  newOwner,SVCIPC_tUserToken token)
{
   SVCIPC_tBool* avail = (SVCIPC_tBool*)(token);

   printf( "[MediaPlayer] newName= ' %s ' oldOwner= ' %s ' newOwner= ' %s\n ",newName,oldOwner,newOwner);

   if ( 0 == strcmp(newOwner, "") )
   {
      *avail = SVCIPC_FALSE;
   }
   else
   {
     *avail = SVCIPC_TRUE;
   }
}


void onNotifyBluetoothOwnerChange(SVCIPC_tConstStr  newName,SVCIPC_tConstStr  oldOwner,SVCIPC_tConstStr  newOwner,SVCIPC_tUserToken token)
{
   SVCIPC_tBool* avail = (SVCIPC_tBool*)(token);


   printf( "[Bluetooth]  newName= ' %s ' oldOwner= ' %s ' newOwner= %s' \n",newName,oldOwner,newOwner);

   if ( 0 == strcmp(newOwner, "") )
   {
      *avail = SVCIPC_FALSE;
   }
   else
   {
      *avail = SVCIPC_TRUE;
   }
}

void onAsyncResult( const SVCIPC_tCallbackStatus* status, SVCIPC_tConstStr   result, SVCIPC_tUserToken   token )
{
   assert( 0 != status );
   if ( SVCIPC_IS_ERROR(status->errCode) &&
     !((SVCIPC_DOMAIN_IPC_LIB == SVCIPC_ERR_GET_DOMAIN(status->errCode)) &&
       (SVCIPC_ERR_CANCELLED == SVCIPC_ERR_GET_CODE(status->errCode))) )
   {
      printf("Async call %s failed 0X %0X",(SVCIPC_tConstStr)token,  status->errCode );
   }

   if ( !SVCIPC_IS_ERROR(status->errCode) )
   {
	  printf( "onAsyncResult: [%s] %s \n",(SVCIPC_tConstStr)token,result);
   }

}
//////////////////////////////////////////////////////////////////////
// Test main

int main()
{

   SVCIPC_tError rc = SVCIPC_initialize();
   SVCIPC_tConnection conn;
   SVCIPC_tSigSubHnd bSub;
   SVCIPC_tSigSubHnd mSub;
   SVCIPC_tSigSubHnd allOwnerSub;
   SVCIPC_tSigSubHnd bOwnerSub;
   SVCIPC_tSigSubHnd mOwnerSub;
   SVCIPC_tHandle invokeHnd;
   SVCIPC_tBool mediaAvail=SVCIPC_FALSE;
   SVCIPC_tBool bluetoothAvail=SVCIPC_FALSE;

  printf( "Welcome to SvcIpcClient \n" );

   signal(SIGTERM, sigHandler);
   signal(SIGINT, sigHandler);
   sleep(1);
   
   if ( SVCIPC_IS_ERROR(rc) )
   {
      printf("Failed to initialize: 0x%0X \n", rc);
   }

   rc = SVCIPC_getConnection(SVCIPC_CONNECTION_SESSION, 0, &conn);
   if ( SVCIPC_IS_ERROR(rc) )
   {
      printf("Failed to get connection: 0x%0X \n", rc);
   }

   rc = SVCIPC_subscribeOwnerChanged(conn, 0 /* listen to all notifications */,
                                    onNotifyAllOwnerChange, 0,
                                    &allOwnerSub);
   if ( SVCIPC_IS_ERROR(rc) )
   {
     printf("Failed to subscribe to 'NameOwnerChanged': 0x%0X \n", rc);
   }

   rc = SVCIPC_subscribeOwnerChanged(conn, "com.harman.service.MediaPlayer",
                                 onNotifyMediaOwnerChange,
                                 (SVCIPC_tUserToken)&mediaAvail,
                                 &mOwnerSub);
   if ( SVCIPC_IS_ERROR(rc) )
   {
     printf("Failed to subscribe to MediaPlayer owner change: 0x%0X \n", rc);
   }

   rc = SVCIPC_nameHasOwner(conn, "com.harman.service.MediaPlayer",
                           (SVCIPC_tBool*)&mediaAvail);
   if ( SVCIPC_IS_ERROR(rc) )
   {
      printf("Failed to initialize MediaPlayer availability flag: 0x%0X \n", rc);
   }

   rc = SVCIPC_subscribeOwnerChanged(conn, "com.harman.service.Bluetooth",
                                 onNotifyBluetoothOwnerChange,
                                 (SVCIPC_tUserToken)&bluetoothAvail,
                                 &bOwnerSub);
   if ( SVCIPC_IS_ERROR(rc) )
   {
     printf("Failed to subscribe to Bluetooth owner change: 0x%0X \n", rc);
   }

   rc = SVCIPC_nameHasOwner(conn, "com.harman.service.Bluetooth",
                           (SVCIPC_tBool*)&bluetoothAvail);
   if ( SVCIPC_IS_ERROR(rc) )
   {
      printf("Failed to initialize Bluetooth availability flag: 0x%0X \n", rc);
   }

   rc = SVCIPC_subscribe(conn,
                           "/com/harman/service/MediaPlayer",
                           "TimeUpdate",
                           onSignal,
                           (SVCIPC_tUserToken)"MediaClient",
                           &mSub);
   if ( SVCIPC_IS_ERROR(rc) )
   {
     printf("Failed to subscribe to 'TimeUpdate' signal: 0x%0X \n", rc);
   }

   rc = SVCIPC_subscribe(conn,
                           "/com/harman/service/Bluetooth",
                           "ConnStatus",
                           onSignal,
                           (SVCIPC_tUserToken)"BluetoothClient",
                           &bSub);
   if ( SVCIPC_IS_ERROR(rc) )
   {
      printf("Failed to subscribe to 'ConnStatus' signal: 0x%0X \n", rc);
   }

   if ( mediaAvail )
   {
      // Let's try a simple asynchronous call
rc =SVCIPC_asyncInvoke(conn,"com.harman.service.MediaPlayer","/com/harman/service/MediaPlayer","play","Peter Gabriel",false,-1,onAsyncResult,&invokeHnd,    "play");

      if ( SVCIPC_IS_ERROR(rc) )
      {
        printf("Failed to asynchronously invoke 'play' on server: 0x%0X \n", rc);
      }

   }

  int counter=0;
   SVCIPC_tResponse* resp;

   while ( 10 > counter )
   {
      if ( mediaAvail )
      {
         rc = SVCIPC_invoke(conn,
                            "com.harman.service.MediaPlayer",
                            "/com/harman/service/MediaPlayer",
                            "play",
                            "Moody Blues",
                            DEFAULT_REQ_TIMEOUT,
                            &resp);

         if ( SVCIPC_IS_ERROR(rc) )
         {
            printf("Failed to invoke 'play' on server: 0x%0X \n", rc);
         }
         else
         {
            printf( "Result: %s ",resp->result);
         }
         SVCIPC_freeResponse(resp);
      }

      if ( bluetoothAvail )
      {
         rc = SVCIPC_invoke(conn,
                            "com.harman.service.Bluetooth",
                            "/com/harman/service/Bluetooth",
                            "pair",
                            "1234",
                            DEFAULT_REQ_TIMEOUT,
                            &resp);

         if ( SVCIPC_IS_ERROR(rc) )
         {
           printf("Failed to invoke 'pair' on server: 0x%0X \n", rc);
         }
         else
         {
           printf( "Result: %s ",resp->result);
         }
         SVCIPC_freeResponse(resp);
      }

      // Sleep and receive signals

   	sleep(1);

   	++counter;
   }

   rc = SVCIPC_unsubscribe(allOwnerSub);
   if ( SVCIPC_IS_ERROR(rc) )
   {
      printf("Failed to unsubscribe from all NotifyOwnerChanged \n");
   }

   rc = SVCIPC_unsubscribe(mOwnerSub);
   if ( SVCIPC_IS_ERROR(rc) )
   {
      printf("Failed to unsubscribe from mediaplayer NotifyOwnerChanged \n");
   }

   rc = SVCIPC_unsubscribe(bOwnerSub);
   if ( SVCIPC_IS_ERROR(rc) )
   {
      printf("Failed to unsubscribe from bluetooth NotifyOwnerChanged \n");
   }

   rc = SVCIPC_unsubscribe(mSub);
   if ( SVCIPC_IS_ERROR(rc) )
   {
     printf("Failed to unsubscribe from Media \n");
   }

   rc = SVCIPC_unsubscribe(bSub);
   if ( SVCIPC_IS_ERROR(rc) )
   {
      printf("Failed to unsubscribe from Bluetooth");
   }

   SVCIPC_closeConnection(conn);
   SVCIPC_shutdown();
return 0;
}
