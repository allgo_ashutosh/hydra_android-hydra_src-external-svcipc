LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_C_INCLUDES:= \
	$(LOCAL_PATH)/../glib-2.0/glib \
	$(LOCAL_PATH)/../glib-2.0 \
	$(LOCAL_PATH)/../json-0.9 \
	$(LOCAL_PATH)/../libsvcipc/include \
	$(LOCAL_PATH)/../dbus-glib/dbus \
	$(LOCAL_PATH)/../dbus-glib \
	$(LOCAL_PATH)/../dbus/dbus \
	$(call include-path-for, dbus)
 
LOCAL_SRC_FILES:= \
	test_main.c 


LOCAL_SHARED_LIBRARIES := libdbus \
 
LOCAL_STATIC_LIBRARIES := libsvcipc 
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE:=svcipc_client

include $(BUILD_EXECUTABLE)
